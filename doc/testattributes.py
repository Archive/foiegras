#!/usr/bin/python

import gtk

from foiegras.attributes import *

mimefield = MIMEField(title='MIME type', default='text/plain', help_text='Select a MIME type')
mimefield.add_filter('text/')
mimefield.apply_filters()

textfield = TextField(title='Some text', default='with default value', help_text='This text should help you...')

dialog = AttributeDialog()
dialog.add_field(mimefield)
dialog.add_field(textfield)
dialog.construct()

dialog.run()
print "DEBUG: values =", dialog.get_field_values()

gtk.main()

