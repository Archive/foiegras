#!/usr/bin/python

import glob
import os.path

from distutils.core import setup

setup(
    name = "Foie Gras",
    version = "0.0.0",
    maintainer = "Szilveszter Farkas",
    maintainer_email = "szilveszter.farkas@gmail.com",
    description = "WYSIWYG Documentation Editor for Project Mallard",
    license = "GNU GPL v2",
    scripts = [ "start-foiegras" ],
    package_dir = {
        "foiegras": os.path.join("src", "foiegras"),
        "foiegras.vcs": os.path.join("src", "foiegras", "vcs"), 
        "foiegras.windows": os.path.join("src", "foiegras", "windows"),
        "foiegras.buffer": os.path.join("src", "foiegras", "buffer")
        },
    packages = [
        "foiegras",
        "foiegras.vcs",
        "foiegras.windows",
        "foiegras.buffer"
        ],
    data_files = [ (os.path.join("share", "foiegras"), glob.glob(os.path.join("data", "*.glade"))),
                   (os.path.join("share", "foiegras"), glob.glob(os.path.join("data", "*.ui"))),
                   (os.path.join("share", "foiegras"), glob.glob(os.path.join("data", "*.xml")))
               ]
)
