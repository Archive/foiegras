# Copyright (C) 2007 by Szilveszter Farkas (Phanatic) <szilveszter.farkas@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import mimetypes

import gobject
import gtk

def _(x):
    return x


class AttributeDialog(gtk.Dialog):
    """ This class is responsible for creating a dialog based on the added
    attribute fields. """
    # No fields at the beginning
    fields = []
    tooltips = gtk.Tooltips()
    
    def __init__(self, parent=None):
        gtk.Dialog.__init__(self,
                            _("Attribute Editor - Foie Gras"),
                            parent,
                            0,
                            ( gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL,
                              gtk.STOCK_APPLY, gtk.RESPONSE_OK )
                        )
        self.tooltips.enable()
    
    def add_field(self, field):
        self.fields.append(field)
    
    def construct(self):
        """ No more fields should be added after calling this method. """
        self._table = gtk.Table(rows=len(self.fields), columns=2)
        # Create labels from titles
        self._labels = []
        for field in self.fields:
            self._labels.append(gtk.Label(field.title))
            if field.help_text is not None:
                self.tooltips.set_tip(field.get_widget(), field.help_text)
        
        # Build up the table
        i = 0
        for field in self.fields:
            self._table.attach(self._labels[i], 0, 1, i, i + 1)
            self._table.attach(field.get_widget(), 1, 2, i, i + 1)
            i += 1
        
        # Set some properties
        self._table.set_row_spacings(3)
        self._table.set_col_spacings(3)
        self._table.set_border_width(5)
        
        self.vbox.pack_start(self._table, True, True)
        self.vbox.show_all()
    
    def get_field_values(self):
        """ Return the field values as a list. """
        values = []
        for field in self.fields:
            values.append(field.get_value())
        
        return values


class BaseField:
    """ Interface for the specific fields. """
    # We default to required value
    required = True
    # No title
    title = None
    # Default help text is None as well
    help_text = None
    # Default value
    default = None
    
    def __init__(self, title, help_text=None, required=True, default=None):
        self.title = title
        self.help_text = help_text
        self.required = required
        self.default = default
        
        self.construct_widget()
        
        if self.default is not None:
            self.set_value(self.default)
    
    def construct_widget(self):
        pass
    
    def get_value(self):
        raise NotImplementedError
    
    def get_widget(self):
        raise NotImplementedError
    
    def set_value(self, value):
        raise NotImplementedError


class MIMEField(BaseField):
    """ Field specific to MIME types. """
    _filters = []
    
    def _apply_filters(self, model, path, iter, user_data):
        type = model.get_value(iter, 0)
        for filter in self._filters:
            if type.startswith(filter):
                user_data.append([ type ])
    
    def add_filter(self, filter):
        """ Add a filter. """
        self._filters.append(filter)
    
    def apply_filters(self):
        """ Apply the given filters. """
        new_model = gtk.ListStore(gobject.TYPE_STRING)
        self._model.foreach(self._apply_filters, new_model)
        self._model = new_model
        self.widget.set_model(self._model)
        
        # Search for the default again, since the model has changed
        if self.default is not None:
            self.set_value(self.default)
    
    def construct_widget(self):
        self.widget = gtk.ComboBox()
        self._model = gtk.ListStore(gobject.TYPE_STRING)
        
        mimetypes.init()
        for ext, type in mimetypes.types_map.iteritems():
            self._model.append([ type ])
        
        cell = gtk.CellRendererText()
        self.widget.pack_start(cell, True)
        self.widget.add_attribute(cell, 'text', 0)
        self.widget.set_model(self._model)
    
    def get_value(self):
        return self._model[self.widget.get_active()][0]
    
    def get_widget(self):
        return self.widget
    
    def set_value(self, value):
        i = 0
        for item in self._model:
            if value == item[0]:
                self.widget.set_active(i)
                break
            i += 1
        else:
            self._model.append(value)
            self.widget.set_active(len(self._model) - 1)


class TextField(BaseField):
    """ Field for basic text input. """
    def construct_widget(self):
        self.widget = gtk.Entry()
    
    def get_value(self):
        return self.widget.get_text()
    
    def get_widget(self):
        return self.widget
    
    def set_value(self, value):
        self.widget.set_text(value)
