# Copyright (C) 2007 by Szilveszter Farkas (Phanatic) <szilveszter.farkas@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import pygtk
pygtk.require('2.0')
import gtk

import foiegras.buffer
from foiegras.buffer import docbuffer

class Document(gtk.VBox):
    """
    This is the default Document abstraction class.
    """

    def __init__(self, filename=None):
        """ Constructor for Document. """
        gtk.VBox.__init__(self)

        self._buffer = docbuffer.DocumentBuffer(filename)

        self._view = gtk.TextView(self._buffer)
        self._view.set_pixels_above_lines(6)
        self._view.set_pixels_below_lines(6)
        self._view.set_left_margin(6)
        self._view.set_right_margin(6)
        self._view.set_wrap_mode(gtk.WRAP_WORD)

        #self._buffer = buffer.MallardBuffer(self._view, "foiegras/buffer/testing.xml")

        self._filename = filename
        self._changed = False

        # Create the ScrolledWindow
        self._scrolledwindow = gtk.ScrolledWindow()
        self._scrolledwindow.set_shadow_type(gtk.SHADOW_IN)
        self._scrolledwindow.add(self._view)
        self._scrolledwindow.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)

        self.add(self._scrolledwindow)
        self.show_all()


    def _set_changed(self, buffer):
        """
        Text has changed in the TextBuffer.
        """
        self._changed = True

    def apply_patch(self, patch_file):
        """
        Applies a patch to the current document - useful for the review mode.
        """
        pass


    def get_buffer(self):
        """
        Returns the TextBuffer.
        """
        return self._buffer


    def get_changed(self):
        """
        Returns True if the text has changed.
        """
        return self._changed


    def get_filename(self):
        """
        Returns the filename of the document.
        """
        return self._filename


    def get_content(self):
        """
        Returns the text of the document (contents of file).
        """
        (start, end) = self._buffer.get_bounds()
        return self._buffer.get_text(start, end)


    def get_view(self):
        """
        Return the TextView.
        """
        return self._view


    def open(filename):
        """
        Static method that returns a Document object if open is
        successful.
        """
        # FIXME: need to work out proper opening, e.g. file checks and throwing
        # exceptions if something went wrong
        doc = Document(filename)
        return doc


    def save(self):
        """
        Save the document.
        """
        if self._filename is not None:
            self._buffer.save(self._filename)
        else:
            print "DEBUG: no filename specified, really shouldn't happen."


    def set_filename(self, filename):
        """
        Specify the document's filename.
        """
        self._filename = filename


class TranslateDocument(Document):
    """
    This class represents a translatable Document (it has a read-only original
    document, and a translatable document in split view).
    """

    def __init__(self, filename, pofile):
        """
        Constructor for TranslateDocument.
        """
        Document.__init__(self, filename)

        self._obuffer = gtk.TextBuffer()
        self._oview = gtk.TextView(self._obuffer)


    def open(filename, pofile):
        """
        Static method that returns a TranslateDocument object if open is
        successful.
        """
        # FIXME: need to work out proper opening, e.g. file checks and throwing
        # exceptions if something went wrong
        doc = TranslateDocument(filename, pofile)
        return doc


    def save(self):
        """
        Save the translation.
        """
        pass
