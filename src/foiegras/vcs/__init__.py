# Copyright (C) 2007 by Szilveszter Farkas (Phanatic) <szilveszter.farkas@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import os.path

from difflib import unified_diff
from tempfile import mkstemp

# List of supported VCSs (module names)
FOIE_GRAS_VCS_TYPES = [ 'bazaar', 'subversion' ]

class VersionControl:
    """ Abstract class for version control backends. """
    def __init__(self):
        """ Constructor for the abstract class. """
        self._url = None
    
    def commit(self, path, message):
        """ Commit the changes to the repository. """
        raise NotImplementedError
    
    def create_diff(self, original, modified, filename):
        """
        Generates a diff between the original and the modified file. Both have
        to be absolute paths.
        
        Returns the path to a temporary file containing the diff.
        """
        # Get the diff generator
        olines = open(original, 'r').readlines()
        mlines = open(modified, 'r').readlines()
        diff = unified_diff(olines,
                            mlines,
                            filename,
                            filename)
        # Create temporary file
        (handler, fullpath) = mkstemp(prefix='foiegras-diff-', text=True)
        # Write contents into the file
        f = file(fullpath, 'w')
        f.writelines(diff)
        f.close()
        # Return the path to the temporary file
        return fullpath
    
    def get_file(self, path):
        """
        Return the path to a temporary file on the local disk that has content
        identical to the one in the repository.
        """
        raise NotImplementedError
    
    def set_url(self, url):
        """ Set repository URL. """
        self._url = url
