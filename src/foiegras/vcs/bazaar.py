# Copyright (C) 2007 by Szilveszter Farkas (Phanatic) <szilveszter.farkas@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

from tempfile import mkstemp

from bzrlib.branch import Branch
from bzrlib.errors import PointlessCommit
from bzrlib.workingtree import WorkingTree

from foiegras.errors import (NoChanges,
                             NoSuchFile,
                             NoWorkingTree,
                             UnknownError)
from foiegras.vcs import VersionControl

class BazaarBackend(VersionControl):
    """ Bazaar VCS backend for Foie Gras. """
    def __init__(self):
        """ Constructor for the Bazaar backend. """
        VersionControl.__init__(self)
        self._branch = None
    
    def commit(self, path, message):
        """
        Commit the given file (path - relative path inside th repository) to the
        Bazaar branch.
        
        Return True if the commit was successful, or the respective error code.
        """
        if self._branch is not None:
            rt = self._branch.repository.revision_tree(self._branch.last_revision())
            id = rt.path2id(path)
            if id is None:
                raise NoSuchFile(path)
            
            try:
                wt = WorkingTree.open(self._url)
            except:
                raise NoWorkingTree(self._url)
            
            try:
                wt.commit(message,
                          allow_pointless=False,
                          strict=False,
                          local=False,
                          specific_files=[path])
                return True
            except PointlessCommit:
                raise NoChanges()
            except:
                raise UnknownError()
        
        # There is no branch at all
        raise NoWorkingTree(self._url)
    
    def get_file(self, path):
        """
        Return a tuple containing the path to a temporary file on the local disk
        that has content identical to the one in the repository, and the
        original filename.
        
        Parameter 'path' has to be a relative path inside the repository.
        
        Return None for both if file was not found.
        """
        if self._branch is not None:
            rt = self._branch.repository.revision_tree(self._branch.last_revision())
            id = rt.path2id(path)
            if id is not None:
                # Valid file id
                content = rt.get_file_text(id)
                # Create temporary file
                (handler, fullpath) = mkstemp(prefix='foiegras-', text=True)
                # Write contents into the file
                f = file(fullpath, 'w')
                f.write(content)
                f.close()
                # Get the original filename
                filename = None
                for item in self._branch.repository.get_inventory(self._branch.last_revision()).entries():
                    if item[0] == path:
                        filename = item[1].name
                        break
                # Return the path to the temporary file
                return (fullpath, filename)
        
        # We didn't succeed
        return (None, None)
    
    def set_url(self, url):
        """ Set branch URL, and get the Branch object. """
        self._url = url
        try:
            self._branch = Branch.open(url)
        except:
            self._branch = None
