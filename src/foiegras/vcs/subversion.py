# Copyright (C) 2007 by Szilveszter Farkas (Phanatic) <szilveszter.farkas@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import os.path

from tempfile import mkstemp

import pysvn

from foiegras.errors import NoWorkingTree
from foiegras.vcs import VersionControl

class SubversionBackend(VersionControl):
    """ Subversion VCS backend for Foie Gras. """
    def __init__(self):
        """ Constructor for the Bazaar backend. """
        VersionControl.__init__(self)
        
        self._client = pysvn.Client()
    
    def commit(self, path, message):
        """
        Commit the given file (path - relative path inside th repository) to the
        Subversion repository.
        
        Return True if the commit was successful
        """
        if self._url is not None:
            revision = self._client.checkin([self._url + path], message)
            print "DEBUG: revision =", revision
            return True
        
        # There is no branch at all
        raise NoWorkingTree(self._url)
    
    def get_file(self, path):
        """
        Return a tuple containing the path to a temporary file on the local disk
        that has content identical to the one in the repository, and the
        original filename.
        
        Parameter 'path' has to be a relative path inside the repository.
        
        Return None for both if file was not found.
        """
        if self._url is not None:
            content = self._client.cat(self._url + path)
            # Create temporary file
            (handler, fullpath) = mkstemp(prefix='foiegras-', text=True)
            # Write contents into the file
            f = file(fullpath, 'w')
            f.write(content)
            f.close()
            # Get the original filename
            filename = os.path.basename(self._url + path)
            # Return the path to the temporary file
            return (fullpath, filename)
        
        # We didn't succeed
        return (None, None)
    
    def set_url(self, url):
        """ Set branch URL, and get the Branch object. """
        if not url.endswith('/'):
            self._url = url + '/'
        else:
            self._url = url
