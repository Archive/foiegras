# Copyright (C) 2007 by Szilveszter Farkas (Phanatic) <szilveszter.farkas@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import os

import pygtk
pygtk.require('2.0')
import gobject
import gtk
import gtk.glade
import pango

try:
    import gtksourceview
    have_gtksourceview = True
except ImportError:
    have_gtksourceview = False

from foiegras.windows.dialog import (AskOverwriteDialog,
                                     error_dialog)

def _(str):
    """ Dummy gettext replacement. """
    return str

class CreatePatchDialog:
    """ A wrapper class around the Create Patch dialog. """
    
    def __init__(self, app):
        """ Constructor for the CreatePatchDialog. """
        self._app = app
        self._conf = self._app.get_conf()
        
        gladefile = self._app.get_datadir() + os.sep + 'patch.glade'
        self._glade = gtk.glade.XML(gladefile, 'dialog_create_patch', 'foiegras')
        
        signals = { 'on_button_cancel_clicked': self.close,
                    'on_button_create_clicked': self.create }
        
        self._glade.signal_autoconnect(signals)
        
        self._window = self._glade.get_widget('dialog_create_patch')
        self._combo_profiles = self._glade.get_widget('combobox_vcsprofile')
        self._entry_relpath = self._glade.get_widget('entry_relpath')
        
        cell = gtk.CellRendererText()
        self._combo_profiles.pack_start(cell, True)
        self._combo_profiles.add_attribute(cell, 'text', 1)
        
        fn = self._app.get_document().get_filename()
        if fn is not None:
            self._entry_relpath.set_text(os.path.basename(fn))
    
    def _get_active_profile(self):
        """ Return the ID of the selected profile or None, if nothing selected. """
        model = self._combo_profiles.get_model()
        active = self._combo_profiles.get_active()
        if active < 0:
            return None
        return model[active][0]
    
    def _load_profiles(self):
        """ This method creates an appropriate ListStore and loads the Version
        Control profiles into it. """
        # model = [ id, name ]
        self._model_vcs = gtk.ListStore(gobject.TYPE_STRING,
                                        gobject.TYPE_STRING)
        
        # Set the model
        self._combo_profiles.set_model(self._model_vcs)
        
        # Retrieve profiles
        profiles = self._conf.get_vcs_profiles()
        
        # Fill up the ListStore
        for p in profiles:
            if p['name'] is not None:
                self._model_vcs.append([ p['id'], p['name'] ])
            while gtk.events_pending():
                gtk.main_iteration()
    
    def close(self, widget=None):
        """ Close button clicked handler. """
        self._window.destroy()
    
    def create(self, widget):
        """ Create Patch button clicked handler. """
        if self._get_active_profile() is None:
            error_dialog(_("No Version Control profile selected"),
                         _("Please select a Version Control profile."))
            return
        if len(self._entry_relpath.get_text()) == 0:
            error_dialog(_("Missing relative path"),
                         _("Please specify the relative path to the original file in the repository."))
            return
        
        data = self._conf.get_vcs_profile(self._get_active_profile())
        if data['type'] == 'bazaar':
            from foiegras.vcs.bazaar import BazaarBackend
            vcs = BazaarBackend()
        elif data['type'] == 'subversion':
            from foiegras.vcs.subversion import SubversionBackend
            vcs = SubversionBackend()
        
        vcs.set_url(data['uri'])
        (origfile, filename) = vcs.get_file(self._entry_relpath.get_text())
        if origfile is None:
            error_dialog(_("File not found"),
                         _("Could not find the original file in the repository."))
            return
        patchfile = vcs.create_diff(origfile, self._app.get_document().get_filename(), filename)
        
        self.close()
        save = SavePatchDialog(self._app, patchfile)
        save.show()
    
    def show(self):
        """ Show all contents of the dialog. """
        self._load_profiles()
        
        self._window.show_all()


class SavePatchDialog:
    """ A wrapper around the Save Patch dialog. """
    
    def __init__(self, app, tempfile):
        """ Constructor for the CreatePatchDialog. """
        self._app = app
        self._conf = self._app.get_conf()
        self._tempfile = tempfile
        
        gladefile = self._app.get_datadir() + os.sep + 'patch.glade'
        self._glade = gtk.glade.XML(gladefile, 'dialog_save_patch', 'foiegras')
        
        signals = { 'on_button_cancel_clicked': self.close,
                    'on_button_save_clicked': self.save }
        
        self._glade.signal_autoconnect(signals)
        
        self._window = self._glade.get_widget('dialog_save_patch')
        self._scrolledwindow = self._glade.get_widget('scrolledwindow_patch')
        
        if have_gtksourceview:
            self._buffer = gtksourceview.SourceBuffer()
            slm = gtksourceview.SourceLanguagesManager()
            gsl = slm.get_language_from_mime_type("text/x-patch")
            self._buffer.set_language(gsl)
            self._buffer.set_highlight(True)
            
            self._textview = gtksourceview.SourceView(self._buffer)
        else:
            self._buffer = gtk.TextBuffer()
            self._textview = gtk.TextView(self._buffer)
        
        self._textview.set_editable(False)
        self._textview.modify_font(pango.FontDescription("Monospace"))
        self._scrolledwindow.add(self._textview)
        
        # Load the contents of the temporary file into the view
        f = open(self._tempfile, 'r')
        self._buffer.set_text(f.read())
        f.close()
   
    def close(self, widget=None):
        """ Close button clicked handler. """
        self._window.destroy()
    
    def save(self, widget):
        """ Save button clicked handler. """
        save_buttons = (gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL,
                        gtk.STOCK_SAVE, gtk.RESPONSE_OK)
        
        filter_diff = gtk.FileFilter()
        filter_diff.set_name(_("Patches and diffs"))
        filter_diff.add_pattern('*.patch')
        filter_diff.add_pattern('*.diff')
        filter_all = gtk.FileFilter()
        filter_all.set_name(_("All files"))
        filter_all.add_pattern('*.*')
        
        dialog = gtk.FileChooserDialog(title=_("Save Patch"),
                                       action=gtk.FILE_CHOOSER_ACTION_SAVE,
                                       buttons=save_buttons)
        dialog.set_default_response(gtk.RESPONSE_OK)
        dialog.add_filter(filter_diff)
        dialog.add_filter(filter_all)
        
        response = dialog.run()
        
        fn = None
        
        if response != gtk.RESPONSE_NONE:
            if response == gtk.RESPONSE_OK:
                fn = dialog.get_filename()
            
            dialog.destroy()
        
        if fn is not None:
            if os.path.exists(fn):
                # We have a problem: file already exists
                dialog = AskOverwriteDialog(fn, self._window)
                
                response = dialog.run()
                
                if response != gtk.RESPONSE_NONE:
                    dialog.destroy()
                    
                    if response == gtk.RESPONSE_CANCEL:
                        return
                else:
                    return
            
            f = open(fn, 'w')
            (start, end) = self._buffer.get_bounds()
            f.write(self._buffer.get_text(start, end))
            f.close()
        
        self.close()
    
    def show(self):
        """ Show all contents of the dialog. """
        self._window.show_all()
