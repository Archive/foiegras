# Copyright (C) 2007 by Szilveszter Farkas (Phanatic) <szilveszter.farkas@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import os

import pygtk
pygtk.require('2.0')
import gobject
import gtk
import gtk.glade

from foiegras.errors import (NoChanges,
                             NoSuchFile,
                             NoWorkingTree,
                             UnknownError)
from foiegras.windows.dialog import error_dialog

def _(str):
    """ Dummy gettext replacement. """
    return str

class CommitDialog:
    """ A wrapper class around the Commit dialog. """
    
    def __init__(self, app):
        """ Constructor for the CreatePatchDialog. """
        self._app = app
        self._conf = self._app.get_conf()
        
        gladefile = self._app.get_datadir() + os.sep + 'commit.glade'
        self._glade = gtk.glade.XML(gladefile, 'dialog_commit', 'foiegras')
        
        signals = { 'on_button_cancel_clicked': self.close,
                    'on_button_commit_clicked': self.commit }
        
        self._glade.signal_autoconnect(signals)
        
        self._window = self._glade.get_widget('dialog_commit')
        self._combo_profiles = self._glade.get_widget('combobox_vcsprofile')
        self._entry_relpath = self._glade.get_widget('entry_relpath')
        self._textview = self._glade.get_widget('textview_message')
        
        cell = gtk.CellRendererText()
        self._combo_profiles.pack_start(cell, True)
        self._combo_profiles.add_attribute(cell, 'text', 1)
        
        fn = self._app.get_document().get_filename()
        if fn is not None:
            self._entry_relpath.set_text(os.path.basename(fn))
    
    def _get_active_profile(self):
        """ Return the ID of the selected profile or None, if nothing selected. """
        model = self._combo_profiles.get_model()
        active = self._combo_profiles.get_active()
        if active < 0:
            return None
        return model[active][0]
    
    def _load_profiles(self):
        """ This method creates an appropriate ListStore and loads the Version
        Control profiles into it. """
        # model = [ id, name ]
        self._model_vcs = gtk.ListStore(gobject.TYPE_STRING,
                                        gobject.TYPE_STRING)
        
        # Set the model
        self._combo_profiles.set_model(self._model_vcs)
        
        # Retrieve profiles
        profiles = self._conf.get_vcs_profiles()
        
        # Fill up the ListStore
        for p in profiles:
            if p['name'] is not None:
                self._model_vcs.append([ p['id'], p['name'] ])
            while gtk.events_pending():
                gtk.main_iteration()
    
    def close(self, widget=None):
        """ Close button clicked handler. """
        self._window.destroy()
    
    def commit(self, widget):
        """ Commit button clicked handler. """
        if self._get_active_profile() is None:
            error_dialog(_("No Version Control profile selected"),
                         _("Please select a Version Control profile."))
            return
        if len(self._entry_relpath.get_text()) == 0:
            error_dialog(_("Missing relative path"),
                         _("Please specify the relative path to the original file in the repository."))
            return
        
        (start, end) = self._textview.get_buffer().get_bounds()
        message = self._textview.get_buffer().get_text(start, end)
        if len(message.strip()) == 0:
            error_dialog(_("Missing message"),
                         _("Please specify a commit message."))
            return
        
        data = self._conf.get_vcs_profile(self._get_active_profile())
        
        if data['readonly']:
            error_dialog(_("Read-only repository"),
                         _("You cannot commit to a read-only repository."))
            return
        
        if data['type'] == 'bazaar':
            from foiegras.vcs.bazaar import BazaarBackend
            vcs = BazaarBackend()
        elif data['type'] == 'subversion':
            from foiegras.vcs.subversion import SubversionBackend
            vcs = SubversionBackend()
        
        vcs.set_url(data['uri'])
        try:
            vcs.commit(self._entry_relpath.get_text(), message)
        except NoWorkingTree, e:
            error_dialog(_("Commit failed"),
                         _("No working tree found at the location:") + ' ' + e.value)
            return
        except NoSuchFile, e:
            error_dialog(_("Commit failed"),
                         _("There is no such file in the repository:") + ' ' + e.value)
            return
        except NoChanges:
            error_dialog(_("Commit failed"),
                         _("There were no changes made to any file."))
            return
        except:
            error_dialog(_("Commit failed"),
                         _("An unknown error has occured."))
            return
        
        self.close()
    
    def show(self):
        """ Show all contents of the dialog. """
        self._load_profiles()
        
        self._window.show_all()
