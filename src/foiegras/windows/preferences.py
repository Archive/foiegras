# Copyright (C) 2007 by Szilveszter Farkas (Phanatic) <szilveszter.farkas@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import os

import pygtk
pygtk.require('2.0')
import gobject
import gtk
import gtk.glade

from foiegras.windows.dialog import error_dialog

def _(str):
    """ Dummy gettext replacement. """
    return str

class PreferencesDialog:
    """ A wrapper class around the Preferences dialog. """
    
    def __init__(self, app):
        """ Constructor for the PreferencesDialog. """
        self._app = app
        self._conf = self._app.get_conf()
        
        gladefile = self._app.get_datadir() + os.sep + 'preferences.glade'
        self._glade = gtk.glade.XML(gladefile, 'dialog_preferences', 'foiegras')
        
        signals = { 'on_button_close_clicked': self.close,
                    'on_button_vcs_add_clicked': self.add_vcs,
                    'on_button_vcs_remove_clicked': self.remove_vcs,
                    'on_button_vcs_edit_clicked': self.edit_vcs }
        
        self._glade.signal_autoconnect(signals)
        
        self._window = self._glade.get_widget('dialog_preferences')
        self._treeview_vcs = self._glade.get_widget('treeview_vcs')
        
        # Add a column to the VCS profiles list
        cell = gtk.CellRendererText()
        self._column_vcs_name = gtk.TreeViewColumn(_("Profile Name"))
        self._column_vcs_name.pack_start(cell, True)
        self._column_vcs_name.add_attribute(cell, 'text', 1)
        self._treeview_vcs.append_column(self._column_vcs_name)
        
        # Register callback for the VCS profiles
        self._conf.add_notify('vcsprofiles', self._notify_callback)
    
    def _get_selected_vcs(self):
        """ Returns the ID of the selected VCS profile. """
        treeselection = self._treeview_vcs.get_selection()
        (model, iter) = treeselection.get_selected()
        
        if iter is None:
            return None
        else:
            return model.get_value(iter, 0)
    
    def _load_profiles(self):
        """ This method creates an appropriate ListStore and loads the Version
        Control profiles into it. """
        # model = [ id, name ]
        self._model_vcs = gtk.ListStore(gobject.TYPE_STRING,
                                        gobject.TYPE_STRING)
        
        # Set the model
        self._treeview_vcs.set_model(self._model_vcs)
        
        # Retrieve profiles
        profiles = self._conf.get_vcs_profiles()
        
        # Fill up the ListStore
        for p in profiles:
            if p['name'] is not None:
                self._model_vcs.append([ p['id'], p['name'] ])
            while gtk.events_pending():
                gtk.main_iteration()
    
    def _notify_callback(self, *args):
        """ Callback function for notifications (reloads profiles). """
        self._load_profiles()
    
    def add_vcs(self, widget):
        """ Add VCS profile button handler. """
        dialog = VCSProfileDialog(self._app)
        dialog.show()
    
    def close(self, widget):
        """ Close button clicked handler. """
        self._window.destroy()
    
    def edit_vcs(self, widget):
        """ Edit VCS profile button handler. """
        sel_id = self._get_selected_vcs()
        if sel_id is not None:
            dialog = VCSProfileDialog(self._app, sel_id)
            dialog.show()
    
    def remove_vcs(self, widget):
        """ Delete VCS profile button handler. """
        if self._get_selected_vcs() is not None:
            dialog = AskRemoveDialog(self._get_selected_vcs, self._window)
            response = dialog.run()
            if response != gtk.RESPONSE_NONE:
                dialog.destroy()
                if response == gtk.RESPONSE_CANCEL:
                    return
            else:
                return
            
            self._conf.remove_vcs_profile(self._get_selected_vcs())
    
    def show(self):
        """ Show all contents of the dialog. """
        self._load_profiles()
        
        self._window.show_all()


from foiegras.vcs import FOIE_GRAS_VCS_TYPES

class VCSProfileDialog:
    """ VCS profile edit/add dialog. """
    
    def __init__(self, app, id=None):
        """ Constructor. If id is None, we'll add a new profile, else edit. """
        self._app = app
        self._conf = self._app.get_conf()
        self._add = True
        
        gladefile = self._app.get_datadir() + os.sep + 'preferences.glade'
        self._glade = gtk.glade.XML(gladefile, 'dialog_vcs_profile', 'foiegras')
        
        signals = { 'on_button_vcs_cancel_clicked': self.close,
                    'on_button_vcs_save_clicked': self.save }
        
        self._glade.signal_autoconnect(signals)
        
        self._window = self._glade.get_widget('dialog_vcs_profile')
        self._entry_id = self._glade.get_widget('entry_vcs_id')
        self._entry_name = self._glade.get_widget('entry_vcs_name')
        self._combo_type = self._glade.get_widget('combobox_vcs_type')
        self._entry_uri = self._glade.get_widget('entry_vcs_uri')
        self._check_readonly = self._glade.get_widget('checkbutton_vcs_readonly')
        
        type = None
        if id is not None:
            # We are in Edit mode
            self._add = False
            # Fetch data
            data = self._conf.get_vcs_profile(id)
            self._entry_id.set_text(data['id'])
            self._entry_name.set_text(data['name'])
            type = data['type']
            self._entry_uri.set_text(data['uri'])
            if data['readonly']:
                self._check_readonly.set_active(True)
        
        if not self._add:
            # Deactivate ID entry, we already have an ID
            self._entry_id.set_sensitive(False)
        
        self._construct_types(type)
    
    def _construct_types(self, selected=None):
        """ Construct the ComboBox model. """        
        self._model_types = gtk.ListStore(gobject.TYPE_STRING)
        for type in FOIE_GRAS_VCS_TYPES:
            self._model_types.append([ type ])
        
        cell = gtk.CellRendererText()
        self._combo_type.pack_start(cell, True)
        self._combo_type.add_attribute(cell, 'text', 0)
        self._combo_type.set_model(self._model_types)
        
        if selected is not None:
            self._combo_type.set_active(FOIE_GRAS_VCS_TYPES.index(selected))
    
    def close(self, widget=None):
        """ Cancel button clicked handler. """
        self._window.destroy()
    
    def save(self, widget):
        """ Save button clicked handler. """
        # Check the entries (they shouldn't be empty)
        if len(self._entry_id.get_text().strip()) == 0:
            error_dialog(_("Missing ID"),
                         _("Please provide an ID for the profile."),
                         self._window)
            return
        if len(self._entry_name.get_text().strip()) == 0:
            error_dialog(_("Missing Name"),
                         _("Please provide a name for the profile."),
                         self._window)
            return
        if self._combo_type.get_active() == -1:
            error_dialog(_("Missing Type"),
                         _("Please select a type for the profile."),
                         self._window)
            return
        if len(self._entry_uri.get_text().strip()) == 0:
            error_dialog(_("Missing URI"),
                         _("Please provide an URI for the profile."),
                         self._window)
            return
        
        if self._add:
            id = self._entry_id.get_text()
            if not id.isalnum():
                error_dialog(_("Invalid ID"),
                             _("The ID must contain only alphanumeric characters (no special ones)."),
                             self._window)
                return
            if self._conf.exists_vcs_profile(id):
                error_dialog(_("Existing ID"),
                             _("The ID already exists, please choose another one."),
                             self._window)
                return
            
        data = {}
        data['id'] = self._entry_id.get_text()
        data['name'] = self._entry_name.get_text()
        data['type'] = FOIE_GRAS_VCS_TYPES[self._combo_type.get_active()]
        data['uri'] = self._entry_uri.get_text()
        data['readonly'] = self._check_readonly.get_active()
        
        if not self._conf.add_vcs_profile(data):
            error_dialog(_("Adding the profile failed"),
                         _("An unknown error happened while adding the profile. Please try again."),
                         self._window)
            return
        
        self.close()
    
    def show(self):
        """ Show all contents of the dialog. """
        self._window.show_all()


class AskRemoveDialog(gtk.Dialog):
    """ This class implements a dialog that asks the user if s/he wants to
    remove the selected profile. """
    def __init__(self, name, parent):
        """ Initialize the question dialog. """
        gtk.Dialog.__init__(self,
                            title=_("Do you want to remove?") + ' - Foie Gras',
                            parent=parent,
                            flags=gtk.DIALOG_MODAL
                        )
        
        # Create the widgets
        self._image_question = gtk.image_new_from_stock(gtk.STOCK_DIALOG_QUESTION,
                                                        gtk.ICON_SIZE_DIALOG)
        self._button_cancel = gtk.Button(stock=gtk.STOCK_CANCEL)
        self._button_remove = gtk.Button(stock=gtk.STOCK_REMOVE)
        self._hbox = gtk.HBox()
        self._label_question = gtk.Label()
        
        # Set properties
        self._hbox.set_spacing(3)
        self._label_question.set_markup(_("<big><b>Do you want to remove the selected profile?</b></big>"))
        
        # Set callbacks
        self._button_cancel.connect('clicked', self._cancel)
        self._button_remove.connect('clicked', self._remove)
        
        # Pack widgets
        self._hbox.pack_start(self._image_question)
        self._hbox.pack_start(self._label_question)
        self.vbox.add(self._hbox)
        self.action_area.pack_start(self._button_cancel)
        self.action_area.pack_start(self._button_remove)
        
        # Show all widgets
        self.vbox.show_all()
        self.action_area.show_all()
    
    def _cancel(self, widget):
        """ Cancel button clicked. """
        self.response(gtk.RESPONSE_CANCEL)
    
    def _remove(self, widget):
        """ Save button click handler. """
        self.response(gtk.RESPONSE_YES)
