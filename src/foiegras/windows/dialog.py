# Copyright (C) 2007 by Szilveszter Farkas (Phanatic) <szilveszter.farkas@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import pygtk
pygtk.require("2.0")

import gtk
import gtk.glade

def _(str):
    """ Dummy gettext replacement. """
    return str

def _message_dialog(type, primary, secondary, buttons=gtk.BUTTONS_OK, parent=None):
    """
    Display a given type of MessageDialog with the given message.
    
    :param type: message dialog type
    
    :param message: the message you want to display.
    """
    dialog = gtk.MessageDialog(flags=gtk.DIALOG_MODAL,
                               type=type,
                               buttons=buttons,
                               parent=parent)
    dialog.set_markup('<big><b>' + primary + '</b></big>')
    dialog.format_secondary_markup(secondary)
    response = dialog.run()
    dialog.destroy()
    return response

def error_dialog(primary, secondary, parent=None):
    """ Display an error dialog with the given message. """
    return _message_dialog(gtk.MESSAGE_ERROR, primary, secondary, parent=parent)

def info_dialog(primary, secondary, parent=None):
    """ Display an info dialog with the given message. """
    return _message_dialog(gtk.MESSAGE_INFO, primary, secondary, parent=parent)

def warning_dialog(primary, secondary, parent=None):
    """ Display a warning dialog with the given message. """
    return _message_dialog(gtk.MESSAGE_WARNING, primary, secondary, parent=parent)


class AskOverwriteDialog(gtk.Dialog):
    """ This class implements a dialog that asks the user if s/he wants to
    overwrite an existing file. """
    def __init__(self, filename, parent):
        """ Initialize the question dialog. """
        gtk.Dialog.__init__(self,
                            title=_("Question") + ' - Foie Gras',
                            parent=parent,
                            flags=gtk.DIALOG_MODAL
                        )
        
        # Create the widgets
        self._image_question = gtk.image_new_from_stock(gtk.STOCK_DIALOG_QUESTION,
                                                        gtk.ICON_SIZE_DIALOG)
        self._button_cancel = gtk.Button(stock=gtk.STOCK_CANCEL)
        self._button_overwrite = gtk.Button("Overwrite")
        self._hbox = gtk.HBox()
        self._label_question = gtk.Label()
        
        # Set properties
        self._hbox.set_spacing(5)
        self._label_question.set_markup(_("<big><b>Do you want to overwrite the old file?</b></big>"))
        
        # Set callbacks
        self._button_cancel.connect('clicked', self._cancel)
        self._button_overwrite.connect('clicked', self._overwrite)
        
        # Pack widgets
        self._hbox.pack_start(self._image_question)
        self._hbox.pack_start(self._label_question)
        self.vbox.add(self._hbox)
        self.action_area.pack_start(self._button_cancel)
        self.action_area.pack_start(self._button_overwrite)
        
        # Show all widgets
        self.vbox.show_all()
        self.action_area.show_all()
    
    def _cancel(self, widget):
        """ Cancel button clicked. """
        print "DEBUG: Cancel button clicked. """
        self.response(gtk.RESPONSE_CANCEL)
    
    def _overwrite(self, widget):
        """ Save button click handler. """
        print "DEBUG: Overwrite button clicked."
        self.response(gtk.RESPONSE_YES)


class AskSaveDialog(gtk.Dialog):
    """ This class implements a dialog that asks the user if s/he wants to save
    the changes made to the document. """
    def __init__(self, filename, parent):
        """ Initialize the question dialog. """
        gtk.Dialog.__init__(self,
                            title=_("Question") + ' - Foie Gras',
                            parent=parent,
                            flags=gtk.DIALOG_MODAL
                        )
        
        if filename is None:
            filename = _("(Unsaved document)")
        
        # Create the widgets
        self._image_question = gtk.image_new_from_stock(gtk.STOCK_DIALOG_QUESTION,
                                                        gtk.ICON_SIZE_DIALOG)
        self._button_dont = gtk.Button(_("Don't Save"))
        self._button_cancel = gtk.Button(stock=gtk.STOCK_CANCEL)
        self._button_save = gtk.Button(stock=gtk.STOCK_SAVE)
        self._hbox = gtk.HBox()
        self._vbox_question = gtk.VBox()
        self._label_question = gtk.Label()
        self._label_filename = gtk.Label()
        
        # Set properties
        self._hbox.set_spacing(5)
        self._hbox.set_border_width(5)
        self._vbox_question.set_spacing(3)
        self._label_question.set_markup(_("<big><b>Do you want to save your changes?</b></big>"))
        self._label_question.set_alignment(0.0, 0.5)
        self._label_filename.set_text(_("The file has been changed:\n") + filename)
        self._label_filename.set_alignment(0.0, 0.5)
        
        # Set callbacks
        self._button_cancel.connect('clicked', self._cancel)
        self._button_dont.connect('clicked', self._dont_save)
        self._button_save.connect('clicked', self._save)
        
        # Pack widgets
        self._vbox_question.pack_start(self._label_question)
        self._vbox_question.pack_start(self._label_filename)
        self._hbox.pack_start(self._image_question)
        self._hbox.pack_start(self._vbox_question)
        self.vbox.add(self._hbox)
        self.action_area.pack_start(self._button_dont)
        self.action_area.pack_start(self._button_cancel)
        self.action_area.pack_start(self._button_save)
        
        # Show all widgets
        self.vbox.show_all()
        self.action_area.show_all()
    
    def _cancel(self, widget):
        """ Cancel button clicked. """
        print "DEBUG: Cancel button clicked. """
        self.response(gtk.RESPONSE_CANCEL)
    
    def _dont_save(self, widget):
        """ Don't Save button click handler. """
        print "DEBUG: Don't Save button clicked."
        self.response(gtk.RESPONSE_NO)
    
    def _save(self, widget):
        """ Save button click handler. """
        print "DEBUG: Save button clicked."
        self.response(gtk.RESPONSE_YES)
