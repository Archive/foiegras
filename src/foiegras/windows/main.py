# Copyright (C) 2007 by Szilveszter Farkas (Phanatic) <szilveszter.farkas@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import os
import os.path

import pygtk
pygtk.require('2.0')
import gobject
import gtk
import gtk.glade

from foiegras.document import Document
from foiegras.windows.commit import CommitDialog
from foiegras.windows.dialog import (AskOverwriteDialog,
                                     AskSaveDialog,
                                     warning_dialog)
from foiegras.windows.preferences import PreferencesDialog
from foiegras.windows.patch import CreatePatchDialog

ACTION_TO_TAG = {
    # block tags
    'CaptionAction'          : 'caption',
    'CitationAction'         : 'cite',
    'MultimediaObjectAction' : 'media',
    'CodeSnippetAction'      : 'code',
    'ParagraphAction'        : 'p',
    'EditorialCommentAction' : 'comment',
    'FigureAction'           : 'figure',
    'ListAction'             : 'list',
    'NoteAction'             : 'note',
    'ScreenAction'           : 'screen',
    'SynopseAction'          : 'synopse',
    'TableAction'            : 'table',

    # inline tags
    'ApplicationNameAction'  : 'app',
    'CommandAction'          : 'cmd',
    'ComputerOutputAction'   : 'output',
    'DateAction'             : 'date',
    'EmphasisAction'         : 'em',
    'FileNameAction'         : 'file',
    'GUILabelAction'         : 'gui',
    'HyperlinkAction'        : 'link',
    'InlineCodeAction'       : 'code-inline',
    'KeyStrokeAction'        : 'key',
    'QuoteAction'            : 'quote',
    'SystemItemAction'       : 'sys',
    'UserInputAction'        : 'input',
    'VariableTextAction'     : 'var',
}

TAG_TO_ACTION = {}
for key in ACTION_TO_TAG.iterkeys():
    TAG_TO_ACTION[ACTION_TO_TAG[key]] = key


def _(str):
    """ Dummy gettext replacement. """
    return str


class OpenRecentToolAction(gtk.Action):
    __gtype_name__ = "OpenRecentToolAction"

gobject.type_register(OpenRecentToolAction)
OpenRecentToolAction.set_tool_item_type(gtk.MenuToolButton)


class MainWindow:
    """
    This is a wrapper class around Foie Gras main window.
    """

    def __init__(self, app, filename=None):
        """
        Constructor for the MainWindow.
        app: a FoieGrasApplication instance
        filename: name of the file that should be opened by default
        """
        self._app = app
        self._conf = self._app.get_conf()
        self._document = None
        self._auto_toggled = False

        gladefile = self._app.get_datadir() + os.sep + 'main.glade'
        self._glade = gtk.glade.XML(gladefile, 'window_main', 'foiegras')

        signals = { 'on_window_main_delete_event': self.close }

        self._glade.signal_autoconnect(signals)

        self._window = self._glade.get_widget('window_main')
        self._vbox = self._glade.get_widget('vbox_main')
        self._hpaned = self._glade.get_widget('hpaned_document')

        # Set up UIManager to load menus and toolbars
        self.uimanager = gtk.UIManager()

        # Set up actions
        actions = [
            ('FileMenuAction', None, _("_File"), None, _(""), self.dummy_action),
            ('EditMenuAction', None, _("_Edit"), None, _(""), self.dummy_action),
            ('ViewMenuAction', None, _("_View"), None, _(""), self.dummy_action),
            ('InsertMenuAction', None, _("_Insert"), None, _(""), self.dummy_action),
            ('AnnotateMenuAction', None, _("_Annotate"), None, _(""), self.dummy_action),
            ('ContributeMenuAction', None, _("_Contribute"), None, _(""), self.dummy_action),
            ('HelpMenuAction', None, _("_Help"), None, _(""), self.dummy_action),
            ('NewAction', gtk.STOCK_NEW, _("_New"), None, _("Create a new document"), self.new_document),
            ('OpenAction', gtk.STOCK_OPEN, _("_Open"), None, _("Open a document"), self.open_document),
            ('RecentlyUsedAction', None, _("Open _Recent"), None, _(""), self.dummy_action),
            ('SaveAction', gtk.STOCK_SAVE, _("_Save"), None, _("Save the current document"), self.save_document),
            ('SaveAsAction', gtk.STOCK_SAVE_AS, _("Save _As..."), "<Ctrl><Shift>S", _(""), self.save_document_as),
            ('DocumentPropertiesAction', gtk.STOCK_PROPERTIES, _("_Document Properties"), None, _(""), self.dummy_action),
            ('QuitAction', gtk.STOCK_QUIT, _("_Quit"), None, _(""), self.quit),
            ('UndoAction', gtk.STOCK_UNDO, _("_Undo"), "<Ctrl>Z", _("Undo the last action"), self.dummy_action),
            ('RedoAction', gtk.STOCK_REDO, _("_Redo"), "<Ctrl>Y", _("Redo the last undone action"), self.dummy_action),
            ('CutAction', gtk.STOCK_CUT, _("Cu_t"), None, _("Cut the selection"), self.cut),
            ('CopyAction', gtk.STOCK_COPY, _("_Copy"), None, _("Copy the selection"), self.copy),
            ('PasteAction', gtk.STOCK_PASTE, _("_Paste"), None, _("Paste the clipboard"), self.paste),
            ('PreferencesAction', gtk.STOCK_PREFERENCES, _("_Preferences"), None, _(""), self.show_preferences),
            ('InsertAction', gtk.STOCK_ADD, _("Insert"), None, _(""), self.dummy_action),
            ('CitationAction', None, _("_Citation"), None, _(""), self.apply_block_tag),
            ('MultimediaObjectAction', None, _("_Multimedia Object..."), None, _(""), self.apply_block_tag),
            ('EditorialCommentAction', None, _("_Editorial Comment"), None, _(""), self.apply_block_tag),
            ('FigureAction', None, _("_Figure..."), None, _(""), self.apply_block_tag),
            ('NoteAction', None, _("_Note"), None, _(""), self.apply_block_tag),
            ('SynopseAction', None, _("_Synopse"), None, _(""), self.apply_block_tag),
            ('TableAction', None, _("_Table..."), None, _(""), self.apply_block_tag),
            ('CreatePatchAction', gtk.STOCK_MISSING_IMAGE, _("Create _Patch"), None, _("Create a patch"), self.create_patch),
            ('CommitAction', gtk.STOCK_MISSING_IMAGE, _("_Commit"), None, _("Commit document changes"), self.commit),
            ('ReviewAction', gtk.STOCK_MISSING_IMAGE, _("_Review"), None, _("Review the current document"), self.dummy_action),
            ('ContentsAction', gtk.STOCK_HELP, _("_Contents"), "F1", _(""), self.dummy_action),
            ('AboutAction', gtk.STOCK_ABOUT, _("_About"), None, _(""), self.about),
            ('ToggleListAction', gtk.STOCK_MISSING_IMAGE, _("Bullets"), None, _("Toggle bullets on/off"), self.dummy_action),
            ('ListIndentAction', gtk.STOCK_INDENT, _("Indent"), None, _("Indent list item"), self.dummy_action),
            ('ListUnindentAction', gtk.STOCK_UNINDENT, _("Unindent"), None, _("Unindent list item"), self.dummy_action)
        ]

        self._annotation_actions = [
            ('ApplicationNameAction', None, _("A_pplication Name"), None, _(""), self.apply_inline_tag),
            ('CommandAction', None, _("_Command"), None, _(""), self.apply_inline_tag),
            ('ComputerOutputAction', None, _("C_omputer Output"), None, _(""), self.apply_inline_tag),
            ('DateAction', None, _("_Date"), None, _(""), self.apply_inline_tag),
            ('EmphasisAction', None, _("E_mphasis"), None, _(""), self.apply_inline_tag),
            ('FileNameAction', None, _("F_ile Name"), None, _(""), self.apply_inline_tag),
            ('GUILabelAction', None, _("_GUI Label"), None, _(""), self.apply_inline_tag),
            ('HyperlinkAction', None, _("H_yperlink"), None, _(""), self.apply_inline_tag),
            ('InlineCodeAction', None, _("I_nline Code"), None, _(""), self.apply_inline_tag),
            ('KeyStrokeAction', None, _("_Key Stroke"), None, _(""), self.apply_inline_tag),
            ('QuoteAction', None, _("_Quote"), None, _(""), self.apply_inline_tag),
            ('SystemItemAction', None, _("_System Item"), None, _(""), self.apply_inline_tag),
            ('UserInputAction', None, _("_User Input"), None, _(""), self.apply_inline_tag),
            ('VariableTextAction', None, _("Va_riable Text"), None, _(""), self.apply_inline_tag),
        ]

        self._toggle_actions = self._annotation_actions + [
            ('SidePaneAction', None, _("_Side Pane"), "F9", _(""), self.toggle_side_pane),        
        ]
        
        self._actiongroup = gtk.ActionGroup('menubar')
        self._actiongroup.add_actions(actions)
        self._actiongroup.add_toggle_actions(self._toggle_actions)
        self._actiongroup.add_action(OpenRecentToolAction("OpenToolAction", _("Open"), _(""), gtk.STOCK_OPEN))

        self.uimanager.insert_action_group(self._actiongroup, 0)
        self._window.add_accel_group(self.uimanager.get_accel_group())

        # Further UIManager stuff
        self.ui = {}
        self.ui['menubar'] = self.uimanager.add_ui_from_file(self._app.get_datadir() + os.sep + 'menubar.ui')
        self.ui['toolbar'] = self.uimanager.add_ui_from_file(self._app.get_datadir() + os.sep + 'toolbar.ui')
        self._menubar = self.uimanager.get_widget('/MainMenu')
        self._menu_recent = self.uimanager.get_widget('/MainMenu/FileMenu/RecentlyUsed')
        self._toolbar_main = self.uimanager.get_widget('/MainToolbar')
        self._tool_open = self.uimanager.get_widget('/MainToolbar/Open')
        self._toolbar_paragraph = self.uimanager.get_widget('/ParagraphToolbar')
        self._toolbar_inline = self.uimanager.get_widget('/InlineToolbar')

        # Initialize Recently Used submenus
        def create_recent_menu():
            recent_menu = gtk.RecentChooserMenu(self._app.recent_manager)
            recent_menu.connect('item-activated', self._recent_item_activated)
            recent_menu.add_filter(self._app.recent_filter)
            return recent_menu

        self._recent_menu = create_recent_menu()
        self._menu_recent.set_submenu(self._recent_menu)
        self._recent_menu2 = create_recent_menu()
        self._tool_open.set_menu(self._recent_menu2)

        self._tool_open.connect('clicked', self._open_toolbar)

        # Toolbar properties
        self._toolbar_paragraph.set_style(gtk.TOOLBAR_ICONS)
        self._toolbar_paragraph.set_property("icon-size", gtk.ICON_SIZE_SMALL_TOOLBAR)
        self._toolbar_inline.set_orientation(gtk.ORIENTATION_VERTICAL)
        self._toolbar_inline.set_style(gtk.TOOLBAR_BOTH_HORIZ)
        self._toolbar_inline.set_border_width(3)

        # Set up paragraph style combo
        self._paragraph_style_combo = gtk.ComboBox()
        parastyle_renderer = gtk.CellRendererText()
        self._paragraph_style_combo.pack_start(parastyle_renderer)
        self._paragraph_style_combo.add_attribute(parastyle_renderer, "text", 0)
        parastyle_model = gtk.ListStore(str, str)
        it = parastyle_model.append()
        parastyle_model.set(it, 0, _("Normal"), 1, "p")
        it = parastyle_model.append()
        parastyle_model.set(it, 0, _("Code Snippet"), 1, "code")
        it = parastyle_model.append()
        parastyle_model.set(it, 0, _("Screen Output"), 1, "screen")
        self._paragraph_style_combo.set_model(parastyle_model)
        self._paragraph_style_combo.connect("changed", self._paragraph_style_changed)
        parastyle_hbox = gtk.HBox(spacing=9)
        parastyle_hbox.set_border_width(3)
        parastyle_label = gtk.Label(_("Paragraph style:"))
        parastyle_label.set_mnemonic_widget(self._paragraph_style_combo)
        parastyle_hbox.pack_start(parastyle_label)
        parastyle_hbox.pack_start(self._paragraph_style_combo)
        parastyle_item = gtk.ToolItem()
        parastyle_item.add(parastyle_hbox)
        self._toolbar_paragraph.insert(parastyle_item, 0)
        self._toolbar_paragraph.insert(gtk.SeparatorToolItem(), 1)

        # Set up hierarchy toolbar
        self._toolbar_hierarchy = gtk.Toolbar()
        hierarchy_label = gtk.Label(_("Hierarchy:"))
        hierarchy_label_item = gtk.ToolItem()
        hierarchy_label_item.set_border_width(3)
        hierarchy_label_item.add(hierarchy_label)
        self._toolbar_hierarchy.add(hierarchy_label_item)
        self._document_button = gtk.RadioButton(label="Document")
        self._document_button.set_mode(False)
        document_button_item = gtk.ToolItem()
        document_button_item.set_border_width(3)
        document_button_item.set_border_width(3)
        document_button_item.add(self._document_button)
        self._toolbar_hierarchy.add(document_button_item)

        # Set up title for annotations toolbar
        inline_label = gtk.Label(_("Annotations"))
        inline_label.set_alignment(0.0, 0.5)
        inline_label.set_padding(3, 6)
        inline_label.set_use_markup(True)

        # Iterate through the ToolItems and set the label alignment accordingly
        num = self._toolbar_inline.get_n_items()
        for i in range(num):
            item = self._toolbar_inline.get_nth_item(i)
            try:
                l = gtk.Label(item.get_label())
                l.set_alignment(0.0, 0.5)
                l.set_padding(3, 0)
                l.set_use_underline(True)
                l.show()
                item.set_label_widget(l)
            except AttributeError:
                pass

        # Pack menus and toolbars into the appropriate places
        self._vbox.pack_start(self._menubar, False, False)
        self._vbox.pack_start(self._toolbar_main, False, False)
        self._vbox.pack_start(self._toolbar_paragraph, False, False)
        self._vbox.pack_start(self._toolbar_hierarchy, False, False)
        self._vbox.reorder_child(self._menubar, 0)
        self._vbox.reorder_child(self._toolbar_main, 1)
        self._vbox.reorder_child(self._toolbar_paragraph, 2)
        self._vbox.reorder_child(self._toolbar_hierarchy, 4)

        # Try to open default document (if n/a, just load a blank one)
        if filename is not None and os.path.exists(filename):
            self._document = Document(filename)
        else:
            self._document = Document()
        self._app.set_document(self._document)

        # Set up side pane
        self._side_pane = gtk.VBox()
        self._side_pane.set_no_show_all(True)
        side_pane_title_hbox = gtk.HBox()
        side_pane_title_hbox.pack_start(inline_label, True, True)
        side_pane_close_button = gtk.Button("")
        side_pane_close_button.set_image(gtk.image_new_from_stock(gtk.STOCK_CLOSE, gtk.ICON_SIZE_MENU))
        side_pane_close_button.set_relief(gtk.RELIEF_NONE)
        side_pane_close_button.connect("clicked", self._side_pane_close_button_clicked)
        side_pane_title_hbox.pack_start(side_pane_close_button, False, False)
        self._side_pane.pack_start(side_pane_title_hbox, False, False)
        side_pane_vport = gtk.Viewport()
        side_pane_vport.set_shadow_type(gtk.SHADOW_IN)
        side_pane_vport.add(self._toolbar_inline)
        self._side_pane.pack_start(side_pane_vport, True, True)
        side_pane_title_hbox.show_all()
        side_pane_vport.show_all()
        
        # Add the document and the notebook to the window
        self._hpaned.pack1(self._document, True, True)
        self._hpaned.pack2(self._side_pane, False, False)

        # Connect signals
        self._document.get_buffer().connect('changed', self._buffer_changed)
        self._document.get_buffer().connect("notify::cursor-position", self._cursor_position_changed)

        self.set_title()

        # The document should grab the focus and place the cursor to the
        # beginning
        self._document.get_view().grab_focus()
        self._document.get_buffer().place_cursor(self._document.get_buffer().get_start_iter())

        # Resize the window
        self._window.resize(self._conf.get_preference('window_width', 'int', 600),
                            self._conf.get_preference('window_height', 'int', 400))
        self._hpaned.set_position(self._conf.get_preference('hpaned_position', 'int', 400))

        # Restore side pane visibility
        show_side_pane = self._conf.get_preference('show_side_pane', 'bool', True)
        self._actiongroup.get_action("SidePaneAction").set_active(show_side_pane)


    def _buffer_changed(self, buffer):
        """
        Text was modified, set title accordingly.
        """
        if self._document.get_filename() is None:
            title = _("(Unsaved Document)")
        else:
            title = os.path.basename(self._document.get_filename())

        self._window.set_title('*%s - Foie Gras' % title)


    def _cursor_position_changed(self, buf, arg):
        """
        Updates the toggle action's state when the cursor position changes.
        """
        tags = buf.get_tags_at_selected_range()

        self._auto_toggled = True

        for action_tuple in self._annotation_actions:
            name = action_tuple[0]
            tag = ACTION_TO_TAG[name]

            self._actiongroup.get_action(name).set_active(tag in tags)

        parastyle_dict = {
            "title": -1,
            "p":      0,
            "code":   1,
            "screen": 2
        }

        for t in tags:
            active = parastyle_dict.get(t, -2)

            if active != -2:
                self._paragraph_style_combo.set_active(active)

        self._toolbar_paragraph.set_sensitive(self._paragraph_style_combo.get_active() != -1)

        self._auto_toggled = False

            
    def _open_toolbar(self, widget):
        self.open_document(None)


    def _recent_item_activated(self, chooser):
        """
        Recently Used item-activated signal handler.
        """
        uri = chooser.get_current_item().get_uri()

        if self._document.get_changed():
            dialog = AskSaveDialog(self._document.get_filename(), self._window)

            response = dialog.run()

            if response != gtk.RESPONSE_NONE:
                dialog.destroy()

                if response == gtk.RESPONSE_YES:
                    self.save_document(None)
                elif response == gtk.RESPONSE_NO:
                    pass
                elif response == gtk.RESPONSE_CANCEL:
                    return

        # Strip 'file://' from the beginning
        filename = uri[7:]

        doc = Document(filename)
        self._app.set_document(doc)
        self.replace_document(doc)

        # Add to Recently Used
        self._app.recent_manager.add_full(uri, self._app.recent_data)


    def _paragraph_style_changed(self, widget):
        """
        Apply block tag according to the chosen paragraph style.
        """
        if not self._auto_toggled:
            tag = widget.get_model().get(widget.get_active_iter(), 1)[0]
            self._document._buffer.apply_block_tag(tag)


    def _side_pane_close_button_clicked(self, widget):
        """
        Close the side pane.
        """
        self._actiongroup.get_action("SidePaneAction").set_active(False)


    def about(self, action):
        """
        About action handler.
        """
        a = gtk.AboutDialog()
        a.set_name('Foie Gras')
        a.set_version(None)
        a.set_copyright('Copyright (c) 2007 Szilveszter Farkas')
        a.set_license("""This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA""")
        a.set_website('http://live.gnome.org/ProjectMallard/FoieGras')
        a.set_authors([ 'Szilveszter Farkas <szilveszter.farkas@gmail.com>' ])

        a.run()
        a.destroy()


    def close(self, widget, event):
        """
        Close window event handler.
        """
        # Check whether document has changed
        if self._document.get_changed():
            dialog = AskSaveDialog(self._document.get_filename(), self._window)

            response = dialog.run()

            if response != gtk.RESPONSE_NONE:
                dialog.destroy()

                if response == gtk.RESPONSE_YES:
                    # Let's save the document
                    self.save_document(None)
                elif response == gtk.RESPONSE_NO:
                    # We don't have to do anything
                    pass
                elif response == gtk.RESPONSE_CANCEL:
                    # We don't want to quit
                    return True

        # Save the window size before quit
        size = self._window.get_size()
        self._conf.set_preference('window_width', 'int', size[0])
        self._conf.set_preference('window_height', 'int', size[1])
        self._conf.set_preference('hpaned_position', 'int', self._hpaned.get_position())

        # End the main loop
        gtk.main_quit()

        return False


    def commit(self, action):
        """
        Commit action handler.
        """
        # We allow committing only if the document was saved
        if self._app.get_document().get_filename() is not None and not self._app.get_document().get_changed():
            dialog = CommitDialog(self._app)
            dialog.show()
        else:
            warning_dialog(_("Committing not allowed"),
                           _("Please save your document before committing."))


    def create_patch(self, action):
        """
        Create Patch action handler.
        """
        # We allow creating patches only if the document was saved
        if self._app.get_document().get_filename() is not None and not self._app.get_document().get_changed():
            dialog = CreatePatchDialog(self._app)
            dialog.show()
        else:
            warning_dialog(_("Creating patch not allowed"),
                           _("Please save your document before creating a patch."))


    def dummy_action(self, action):
        """
        Dummy action handler.
        """
        pass


    def new_document(self, action):
        """
        New document.
        """
        if self._document.get_changed():
            dialog = AskSaveDialog(self._document.get_filename(), self._window)

            response = dialog.run()

            if response != gtk.RESPONSE_NONE:
                dialog.destroy()

                if response == gtk.RESPONSE_YES:
                    self.save_document(None)
                elif response == gtk.RESPONSE_NO:
                    pass
                elif response == gtk.RESPONSE_CANCEL:
                    return

        doc = Document()
        self._app.set_document(self._document)
        self.replace_document(doc)


    def open_document(self, action):
        """
        OpenAction handler.
        """
        if self._document.get_changed():
            dialog = AskSaveDialog(self._document.get_filename(), self._window)

            response = dialog.run()

            if response != gtk.RESPONSE_NONE:
                dialog.destroy()

                if response == gtk.RESPONSE_YES:
                    self.save_document(None)
                elif response == gtk.RESPONSE_NO:
                    pass
                elif response == gtk.RESPONSE_CANCEL:
                    return

        open_buttons = (gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL,
                        gtk.STOCK_OPEN, gtk.RESPONSE_OK)

        filter = gtk.FileFilter()
        filter.set_name(_("Project Mallard XML files"))
        filter.add_pattern('*.xml')
        filter_all = gtk.FileFilter()
        filter_all.set_name(_("All files"))
        filter_all.add_pattern('*')

        dialog = gtk.FileChooserDialog(title=_("Open Document"),
                                       action=gtk.FILE_CHOOSER_ACTION_OPEN,
                                       buttons=open_buttons)
        dialog.set_default_response(gtk.RESPONSE_OK)
        dialog.add_filter(filter)
        dialog.add_filter(filter_all)

        response = dialog.run()

        if response != gtk.RESPONSE_NONE:
            if response == gtk.RESPONSE_OK:
                doc = Document(dialog.get_filename())
                self._app.set_document(doc)
                self.replace_document(doc)

                # Add to Recently Used
                self._app.recent_manager.add_full('file://' + dialog.get_filename(),
                                                  self._app.recent_data)

            dialog.destroy()


    def quit(self, action):
        """
        QuitAction handler.
        """
        # Check whether document has changed
        if self._document.get_changed():
            dialog = AskSaveDialog(self._document.get_filename(), self._window)

            response = dialog.run()

            if response != gtk.RESPONSE_NONE:
                dialog.destroy()

                if response == gtk.RESPONSE_YES:
                    # Let's save the document
                    self.save_document(None)
                elif response == gtk.RESPONSE_NO:
                    # We don't have to do anything
                    pass
                elif response == gtk.RESPONSE_CANCEL:
                    # We don't want to quit
                    return

        # Save the window size before quit
        size = self._window.get_size()
        self._conf.set_preference('window_width', 'int', size[0])
        self._conf.set_preference('window_height', 'int', size[1])
        self._conf.set_preference('hpaned_position', 'int', self._hpaned.get_position())

        # End the main loop
        gtk.main_quit()


    def replace_document(self, new_document):
        """
        Replace the current document in the main window with the document
        set in the application instance.
        """
        self._document.destroy()
        # Assign the new document
        self._document = new_document
        self._app.set_document(self._document)
        # Pack the new document into the main window
        self._hpaned.add1(self._document)
        # Show the document itself
        self._document.show_all()

        # Connect signals
        self._document.get_buffer().connect('changed', self._buffer_changed)
        self._document.get_buffer().connect("notify::cursor-position", self._cursor_position_changed)

        self.set_title()

        # The document should grab the focus and place the cursor to the
        # beginning
        self._document.get_view().grab_focus()
        self._document.get_buffer().place_cursor(self._document.get_buffer().get_start_iter())


    def save_document(self, action):
        """
        SaveAction handler.
        """
        doc = self._app.get_document()

        if doc.get_filename() is not None:
            doc.save()
            self.set_title()
        else:
            self.save_document_as(action)


    def save_document_as(self, action):
        """
        SaveAsAction handler.
        """
        doc = self._app.get_document()

        save_buttons = (gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL,
                        gtk.STOCK_SAVE, gtk.RESPONSE_OK)

        filter = gtk.FileFilter()
        filter.set_name(_("Project Mallard XML files"))
        filter.add_pattern('*.xml')
        filter_all = gtk.FileFilter()
        filter_all.set_name(_("All files"))
        filter_all.add_pattern('*')

        dialog = gtk.FileChooserDialog(title=_("Save Document"),
                                       action=gtk.FILE_CHOOSER_ACTION_SAVE,
                                       buttons=save_buttons)
        dialog.set_default_response(gtk.RESPONSE_OK)
        dialog.add_filter(filter)
        dialog.add_filter(filter_all)

        response = dialog.run()

        fn = None

        if response != gtk.RESPONSE_NONE:
            if response == gtk.RESPONSE_OK:
                fn = dialog.get_filename()
                if fn[-4:] != '.xml':
                    fn = fn + '.xml'

            dialog.destroy()

        if fn is not None:
            if os.path.exists(fn):
                # We have a problem: file already exists
                dialog = AskOverwriteDialog(self._document.get_filename(), self._window)

                response = dialog.run()

                if response != gtk.RESPONSE_NONE:
                    dialog.destroy()

                    if response == gtk.RESPONSE_CANCEL:
                        return
                else:
                    return

            doc.set_filename(fn)
            doc.save()

            # Add to Recently Used
            self._app.recent_manager.add_full('file://' + fn,
                                              self._app.recent_data)

            self.set_title()


    def set_title(self):
        """
        Put the filename into the main window's title.
        """
        if self._document.get_filename() is None:
            title = _('(Unsaved Document)')
        else:
            title = os.path.basename(self._document.get_filename())

        self._window.set_title(title + ' - Foie Gras')


    def show(self):
        """
        Display the window.
        """
        self._window.show_all()


    def cut(self, action):
        """
        Cut the selected text.
        """
        self._document._buffer.cut_clipboard(gtk.Clipboard(), True)


    def copy(self, action):
        """
        Copy the selected text.
        """
        self._document._buffer.copy_clipboard(gtk.Clipboard())


    def paste(self, action):
        """
        Paste the clipboard.
        """
        self._document._buffer.paste_clipboard(gtk.Clipboard(), None, True)


    def show_preferences(self, action):
        """
        Show the Preferences dialog.
        """
        pref = PreferencesDialog(self._app)
        pref.show()


    def toggle_side_pane(self, action):
        """
        Show or hide the side pane.
        """
        show = action.get_active()

        if action.get_active() == True:
            self._side_pane.show()
        else:
            self._side_pane.hide()

        self._conf.set_preference('show_side_pane', 'bool', show)


    def apply_block_tag(self, action):
        """
        Applies a block tag to the current selection.
        """
        tag = ACTION_TO_TAG[action.get_name()]
        
        self._document._buffer.apply_block_tag(tag)


    def apply_inline_tag(self, action):
        """
        Applies an inline tag to the current selection.
        """
        if not self._auto_toggled:
            tag = ACTION_TO_TAG[action.get_name()]

            if action.get_active():
              self._document._buffer.apply_inline_tag(tag)
            else:
                self._document._buffer.remove_inline_tag(tag)

            self._document.get_view().grab_focus()
