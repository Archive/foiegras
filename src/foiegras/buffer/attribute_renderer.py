# Copyright (C) 2007 by Buddhika Laknath <blaknath@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""
attribute_renderer:
Render the attributes.
Will be called by attribute_handler after validating attributes
"""

import pygtk
pygtk.require('2.0')
import gtk

def media(buffer,href,textview):
    """render media tags"""
    buffer.insert(buffer.get_end_iter(), "\n")

    #insert image
    image = gtk.Image()
    image.set_from_file(href)
    buffer.insert_pixbuf(buffer.get_end_iter(), image.get_pixbuf())
    textview.height += image.get_pixbuf().get_height()

    #start the new tag in a new line
    buffer.insert(buffer.get_end_iter(), "\n")    
