# Copyright (C) 2008 by Denis Washington <denisw@svn.gnome.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import xml.parsers.expat

from tags import BLOCK_TAGS, INLINE_TAGS, get_text_tag

def open_document(doc_buffer):
    """
    Opens the passed document buffer.
    """
    DocumentLoader(doc_buffer).parse()


class DocumentLoader (object):

    def __init__(self, doc_buffer):
        self.buffer = doc_buffer
        self._active_tags = []
        self._tag_names = []


    def parse(self):
        parser = xml.parsers.expat.ParserCreate()

        parser.StartElementHandler = self._start_element
        parser.EndElementHandler = self._end_element
        parser.CharacterDataHandler = self._char_data

        f = file(self.buffer.get_filename())
        parser.ParseFile(f)
        f.close()


    def _start_element(self, name, attrs):
        if name in BLOCK_TAGS or name in INLINE_TAGS:
            self._active_tags.append(get_text_tag(name))
            self._tag_names.append(name)


    def _end_element(self, name):
        if name in BLOCK_TAGS or name in INLINE_TAGS:
            self._active_tags.remove(get_text_tag(name))
            self._tag_names.remove(name)

        if name in BLOCK_TAGS:
            self.buffer.insert_with_tags(self.buffer.get_end_iter(), "\n", *self._active_tags)


    def _char_data(self, data):
        print "'%s': %s" % (data, self._tag_names)
        self.buffer.insert_with_tags(self.buffer.get_end_iter(), data, *self._active_tags)


