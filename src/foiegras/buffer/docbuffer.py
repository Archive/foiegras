# Copyright (C) 2008 by Denis Washington <denisw@svn.gnome.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import pygtk
pygtk.require('2.0')
import gtk
import gobject

from block_handler import block_handler
from tags import INLINE_TAGS, BLOCK_TAGS, SIMPLE_BLOCK_TAGS, TAG_TABLE, get_text_tag
from open import open_document
from save import save_document

def _(string):
    return string

class DocumentBuffer (gtk.TextBuffer):
    """
    Represents a Mallard document.
    """

    def __init__(self, filename = None):
        """
        Creates a Document.
        """
        gtk.TextBuffer.__init__(self, TAG_TABLE)

        self.__filename = filename
        self.__activated_tags = set()
        self.__deactivated_tags = set()
        self.__manual_insert = False

        self.connect_after("insert-text", self.__insert_text_after_cb)
     
        if filename:
            # TODO: File loading
            open_document(self)
        else:
            self.set_text(_("New Topic\nAdd the topic's content here."))

            # NOTE: This is needed so that the block
            # handler does not get confused
            self.select_range(self.get_start_iter(), self.get_start_iter())

            start_iter = self.get_start_iter()
            self.apply_block_tag("title", start_iter)

            newline_iter = self.get_start_iter().copy()

            while not newline_iter.ends_line():
                newline_iter.forward_char()

            p_start = newline_iter.copy()
            p_start.forward_char()
            p_end = self.get_end_iter()

            self.select_range(p_start, p_end)
            self.apply_block_tag("p", p_start)

            newline_iter = self.get_start_iter().copy()

            while not newline_iter.ends_line():
                newline_iter.forward_char()

            end_iter = newline_iter.copy()
            end_iter.forward_char()

            self.apply_tag(self.get_tag_table().lookup("_readonly"), newline_iter, end_iter)


    def get_filename(self):
        """
        Returns the name of the file represented by the buffer.
        """
        return self.__filename


    def save(self, filename = None):
        """
        Saves the document buffer's content. If no file name is given,
        the document is saved at the location referred to by the the
        return value of the buffer's get_filename() method.
        """
        if filename:
            self.__filename = filename

        save_document(self)


    def apply_block_tag(self, tag_name, block_iter = None):
        """
        Applies a block tag to the block in which the passed text iterator
        is located. If no iterator is given, the tag is applied to the block
        at the current cursor position.
        """
        block_handler(self, tag_name, block_iter, False)


    def insert_block_tag(self, tag_name, start_iter = None):
        """
        Inserts a new a block tag at the passed position. If no iterator is
        given, the block is inserted at the current cursor position.
        """
        block_handler(self, tag_name, start_iter, True)


    def apply_inline_tag(self, tag_name, start_iter = None, end_iter = None):
        """
        Applies an inline tag to the passed range. If both iterators point at
        the same spot in the buffer, the tag is "activated", meaning that
        it is applied to subsequently inserted text until deactivated. If no
        iterators are given, the tag is applied to the currently selected range
        (or activated at the current cursor position).
        """
        if not start_iter:
            start_iter = self.get_iter_at_mark(self.get_insert())
        if not end_iter:
            end_iter = self.get_iter_at_mark(self.get_selection_bound())

        if start_iter.equal(end_iter):
            self.__set_tag_active(tag_name, True)
        else:
            self.apply_tag_by_name(tag_name, start_iter, end_iter)


    def remove_inline_tag(self, tag_name, start_iter = None, end_iter = None):
        """
        Removes an inline tag from the passed range. If both iterators point at
        the same spot in the buffer, the tag is deactivated (see doc comment of
        apply_inline_tag()). If no iterators are given, the tag is removed from
        the currently selected range (or deactivated at the current cursor
        position).
        """
        if not start_iter:
            start_iter = self.get_iter_at_mark(self.get_insert())
        if not end_iter:
            end_iter = self.get_iter_at_mark(self.get_selection_bound())

        if start_iter.equal(end_iter):
            self.__set_tag_active(tag_name, False)
        else:
            self.remove_tag_by_name(tag_name, start_iter, end_iter)


    def __set_tag_active(self, tag_name, active):
        """
        Manually activates or deactivates a tag. (See apply_inline_tag().)
        """
        if active:
            self.__activated_tags.add(tag_name)

            if tag_name in self.__deactivated_tags:
                self.__deactivated_tags.remove(tag_name)
                
        else:
            self.__deactivated_tags.add(tag_name)

            if tag_name in self.__activated_tags:
                self.__activated_tags.remove(tag_name)


    def get_tags_at_selected_range(self):
        """
        Returns the names of all tags applied to the currently selected
        text range or, if nothing is selected, of the current cursor
        position.
        """
        bounds = self.get_selection_bounds()

        if len(bounds) == 0:
            start = end = self.get_iter_at_mark(self.get_insert())
            start.backward_char()
        else:
            start = bounds[0]
            end = bounds[1] 

        tags = set()

        for tag in start.get_tags():
            tags.add(tag.get_property("name"))

        if start.equal(end):
            tags = tags.union(self.__activated_tags)
        else:
            it = start.copy()

            while not it.equal(end):
                applied = set()

                for tag in it.get_tags():
                    applied.add(tag.get_property("name"))

                tags = tags.intersection(applied)
                it.forward_char()

        has_block_tag = False

        for t in tags:
            if t in BLOCK_TAGS:
                has_block_tag = True

        if not has_block_tag:
            newline = start.copy()

            if not newline.ends_line():
                newline.forward_to_line_end()

            if newline.has_tag(get_text_tag("title")):
                tags.add("title")
            else:
                tags.add("p")

        return tags


    def insert_with_tags(self, textiter, data, *tags):
        """
        """
        self.__manual_insert = True
        gtk.TextBuffer.insert_with_tags(self, textiter, data, *tags)
        self.__manual_insert = False


    def __insert_text_after_cb(self, textbuffer, location, text, length):
        """
        "insert-text" signal handler. (Runs after the default hander).
        """
        if not self.__manual_insert:
            num_lines_back = 0

            for i in range(length):
                if text[i] == "\n" or (text[i] == "\r" and text[i + 1] != "\n"):
                     num_lines_back += 1

            for i in range(num_lines_back):
                location.backward_line()

            start_iter = location.copy()
            start_iter.backward_chars(length)

            for tag_name in self.__get_active_tags(start_iter):
                self.apply_tag_by_name(tag_name, start_iter, location)

            self.notify("cursor-position")


    def __get_active_tags(self, location):
        """
        Returns the active tags at the passed text iterator.
        """
        active_tags = set()

        it = location.copy()
        it.backward_char()

        for tag in it.get_tags():
            tag_name = tag.get_property("name")

            if not tag_name.startswith("_") and \
            not (it.ends_line() and tag_name in BLOCK_TAGS):
                active_tags.add(tag.get_property("name"))

        active_tags = active_tags.union(self.__activated_tags)
        active_tags = active_tags.difference(self.__deactivated_tags)

        self.__activated_tags.clear()
        self.__deactivated_tags.clear()

        if len(active_tags) == 0:
            newline = location.copy()

            if not newline.ends_line():
                newline.forward_to_line_end()

            if newline.has_tag(get_text_tag("title")):
                active_tags.add("title")
            else:
                active_tags.add("p")

        return active_tags

