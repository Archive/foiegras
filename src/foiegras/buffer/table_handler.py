# Copyright (C) 2007 by Buddhika Laknath <blaknath@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import pygtk
pygtk.require('2.0')
import gtk


def handler(parser, element, attrs, tag_start, tag = None):
    """
    Handler for table related tags.
    This will decide how table, tr, td, thead, tbody tags 
    will be displayed.
    """     
    #choose the current focused text view
    if parser.main_buffer.inner_textviews:
        current_textview = parser.main_buffer.inner_textviews[len(parser.main_buffer.inner_textviews)-1]
    else:
        current_textview = parser.main_buffer.main_textview
               
    #when tag is starting  
    if tag_start is True:     
        #when a table tag is found
        #start counting the columns        
        if element == "table":
            parser.table_status = parser.table_cons["TABLE_DEFINED"]
        elif element == "tr":
            parser.last_row = []
            parser.num_columns = 0            
        
    #if tag is closing
    else:
        #handling tr tag
        if element == "tr":         
            #if table tag is found earlier and tr is found    
            if parser.table_status ==  parser.table_cons["TABLE_DEFINED"]:
                #create a list store for each of column in str type
                liststore = gtk.ListStore(*[str for x in range(parser.num_columns)])
                liststore.append(parser.last_row)

                #create a treeview from list store
                parser.current_table = gtk.TreeView(liststore) 
                parser.current_table.set_headers_visible(False)
                parser.current_table.set_grid_lines(gtk.TREE_VIEW_GRID_LINES_BOTH)
                parser.current_table.set_border_width(4)
    
                # create the TreeViewColumns to display the data
                columns = [gtk.TreeViewColumn() for x in range(parser.num_columns)]

                # create a list of cellrenders
                cell_renders = [gtk.CellRendererText() for x in range(parser.num_columns)]

                # add columns to treeview
                # add cells to columns
                # set the cell attributes to the appropriate liststore column
                for x in range(parser.num_columns):
                    parser.current_table.append_column(columns[x])
                    columns[x].pack_start(cell_renders[x],True)
                    columns[x].set_attributes(cell_renders[x],text=x)
                    
                # get the text iter to position a new textview
                iter = parser.temp_buffer.get_end_iter()            
                anchor  = parser.temp_buffer.create_child_anchor(iter)
                current_textview.add_child_at_anchor(parser.current_table,anchor)
                parser.current_table.show_all()
                              
                parser.table_status = parser.table_cons["TABLE_DRAWN"]  

            # if table is drawn, add each row as table processes
            elif parser.table_status ==  parser.table_cons["TABLE_DRAWN"]:          
                # append data to the list store to create a new row           
                parser.current_table.get_model().append(parser.last_row) 

        # increase the number of columns by 1 when td tag closed               
        elif element == "td":
            parser.num_columns += 1         
      
        # close table tag and status of table
        elif element == "table":
            parser.table_status = parser.table_cons["NO_TABLE"]    
            width,height =  parser.current_table.size_request()
            parser.temp_block_height += height
                  
