# Copyright (C) 2007 by Buddhika Laknath <blaknath@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import pygtk
pygtk.require('2.0')
import sys, os, errno
import gtk,table


class MallardTable(gtk.TextTagTable):
    """
    The main buffer for text editor
    """
        
    def __init__(self):
        gtk.TextTagTable.__init__(self)
        #print 'testing'
        
    def addTags(self,attrbs = dict() ):
        """
        Creates a new tag and adds it to the TextTagTable
        """    
        attrbs = set(attrbs)
        tag = gtk.TextTag()
        
        if type(attrbs).__name__ == 'dict':
            for key,val in attrbs.iteritems():
                tag.key = val
        
        self.add(tag) 
        
    def getTable(self):
        return self
        
    
        
        
            
                
           
            
        
