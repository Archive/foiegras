# Copyright (C) 2007 by Buddhika Laknath <blaknath@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

"""
attribute_handler:
Validate attributes.
Call attribute_renderer to render tags.
"""

import pygtk
pygtk.require('2.0')
import gtk
import re,sys
import attribute_renderer

def attribute_handler(tag, buffer, tag_table, attribs, textview):
    """
    Handler for validating and rendering tag attributes 
    """  
    #check for given attributes with valid attributes
    for k,v in attribs.iteritems():
        if k.strip() in tag_table:
            valid = [x.strip() for  x in tag_table[k].split("|")]

            #Valid can be a UPPER CASED CONSTANT eg: PATH_STRING
            #or can be a valid attrib set eg: image/png | image/gif | image/jpg
            if len(valid) == 1 and valid[0].isupper():
                #if attribute is a constant it'll validated here
                if valid[0] == "PATH_STRING":
                    if re.compile("[A-Za-z0-9/.//_+-]+\.[A-Za-z0-9//_+-]{1}" ).match(v) == None:
                        raise InvlidAttribute("Invalid pattern for attribute '" + k + "' in '" + tag + "'")
                if valid[0] == "URL_STRING":
                    if re.compile("http://[A-Za-z0-9/.//_+-]+\.[A-Za-z0-9//_+-]{1}" ).match(v) == None:
                        raise InvlidAttribute("Invalid pattern for attribute '" + k + "' in '" + tag + "'")                    
            else:
                #if attribute is a set of values it'll be checked here 
                #eg: image/png | image/gif | image/jpg
                if v.strip() not in valid:
                    raise InvlidAttribute(v + ": Wrong type for attribute " + k + " in " + tag)
        else:
            raise InvlidAttribute("Invalid attribute " + k + " for " + tag)
    
    #branch according to the parent tag and render accordingly        
    if tag == "media":
        #for media tag
        if "href" in attribs:
            attribute_renderer.media(buffer, attribs["href"], textview)

    if tag == "table":
        #for table tag
        print "table tag attributes are not yet implemented."
        pass
        
#User defined exception for invalid attributes
class InvlidAttribute(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)    
