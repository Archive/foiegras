# Copyright (C) 2008 by Denis Washington <denisw@svn.gnome.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

from xml.dom.minidom import getDOMImplementation
from StringIO import StringIO

from tags import BLOCK_TAGS, SIMPLE_BLOCK_TAGS, INLINE_TAGS, get_text_tag

def save_document(doc_buffer):
    """
    Saves the passed document buffer.
    """
    impl = getDOMImplementation()

    doc = impl.createDocument(None, "topic", None)

    textiter = doc_buffer.get_start_iter()

    while not textiter.is_end():
        for tag_name in BLOCK_TAGS:
            if textiter.has_tag(get_text_tag(tag_name)):
                doc.documentElement.appendChild(handle_simple_block_tag(tag_name, textiter, doc))
                break

    f = file(doc_buffer.get_filename(), "w+")
    doc.writexml(f)
    f.close()


def handle_simple_block_tag(tag_name, textiter, doc):
    """
    Handles a simple block tag.
    """
    e = doc.createElement(tag_name)
    text_io = StringIO()

    while not (textiter.ends_line() or textiter.is_end()):
        inline_tag_found = False

        for itag in INLINE_TAGS:
            if textiter.has_tag(get_text_tag(itag)):
                text_io = _add_buffered_text_to_element(text_io, e, doc)
                e.appendChild(handle_inline_tag(itag, textiter, doc))
                inline_tag_found = True
                break

        if not inline_tag_found:
            text_io.write(textiter.get_char())
            textiter.forward_char()

    _add_buffered_text_to_element(text_io, e, doc)
    textiter.forward_char()
    return e


def handle_inline_tag(tag_name, textiter, doc):
    e = doc.createElement(tag_name)
    text_io = StringIO()

    while textiter.has_tag(get_text_tag(tag_name)) and not textiter.is_end():
        inline_tag_begins = False

        for itag in INLINE_TAGS:
            if itag != tag_name:
                if textiter.begins_tag(get_text_tag(itag)):
                    text_io = _add_buffered_text_to_element(text_io, e, doc)
                    e.appendChild(handle_inline_tag(itag, textiter, doc))
                    inline_tag_begins = True
                    break
                elif textiter.ends_tag(get_text_tag(itag)):
                    _add_buffered_text_to_element(text_io, e, doc)
                    if e.hasChildNodes():
                        return e

        if not inline_tag_begins:
            text_io.write(textiter.get_char())
            textiter.forward_char()

    _add_buffered_text_to_element(text_io, e, doc)
    return e


def _add_buffered_text_to_element(text_io, e, doc):
    """
    Appends the text in the passed StringIO to the specified element as
    text node, and returns a fresh text node.
    """
    text = text_io.getvalue()

    if len(text) > 0:
        text_io.close()
        text_io = StringIO()
        e.appendChild(doc.createTextNode(text))

    return text_io

