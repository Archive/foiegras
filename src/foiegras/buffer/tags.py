# Copyright (C) 2008 by Denis Washington <denisw@svn.gnome.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import pygtk
pygtk.require('2.0')
import gtk
import pango

INLINE_TAGS = []
BLOCK_TAGS = []
SIMPLE_BLOCK_TAGS = []
ATTRIBUTES = []

TAG_TABLE = gtk.TextTagTable()


def get_text_tag(tag_name):
    return TAG_TABLE.lookup(tag_name)


def _setup_tags():

    def block_tag(name):
        BLOCK_TAGS.append(name)
        return gtk.TextTag(name)

    def simple_block_tag(name):
        SIMPLE_BLOCK_TAGS.append(name)
        return block_tag(name)

    def inline_tag(name):
        INLINE_TAGS.append(name)
        return gtk.TextTag(name)

    # read-only areas
    tag = gtk.TextTag("_readonly")
    tag.set_property("editable", False)
    TAG_TABLE.add(tag)

    # <app>
    tag = inline_tag("app")
    TAG_TABLE.add(tag)

    # <caption>
    tag = block_tag("caption")
    TAG_TABLE.add(tag)

    # <cmd>
    tag = inline_tag("cmd")
    tag.set_property("family", "monospace")
    TAG_TABLE.add(tag)

    # <code>
    tag = simple_block_tag("code")
    tag.set_property("family", "monospace")
    TAG_TABLE.add(tag)

    # <code-inline>
    tag = inline_tag("code-inline")
    tag.set_property("family", "monospace")
    TAG_TABLE.add(tag)

    # <cite>
    tag = block_tag("cite")
    tag.set_property("style", pango.STYLE_ITALIC)
    TAG_TABLE.add(tag)

    # <date>
    tag = inline_tag("date")
    tag.set_property("style", pango.STYLE_ITALIC)
    TAG_TABLE.add(tag)

    # <em>
    tag = inline_tag("em")
    tag.set_property("weight", pango.WEIGHT_BOLD)
    TAG_TABLE.add(tag)

    # <figure>
    tag = block_tag("figure")
    TAG_TABLE.add(tag)

    # <file>
    tag = inline_tag("file")
    tag.set_property("style", pango.STYLE_ITALIC)
    TAG_TABLE.add(tag)

    # <gui>
    tag = inline_tag("gui")
    tag.set_property("style", pango.STYLE_ITALIC)
    TAG_TABLE.add(tag)

    # <input>
    tag = inline_tag("input")
    tag.set_property("family", "monospace")
    TAG_TABLE.add(tag)

    # <key>
    tag = inline_tag("key")
    tag.set_property("style", pango.STYLE_ITALIC)
    TAG_TABLE.add(tag)

    # <link>
    tag = inline_tag("link")
    tag.set_property("foreground", "#0000ff")
    tag.set_property("underline", True)
    TAG_TABLE.add(tag)

    # <list>
    tag = block_tag("list")
    TAG_TABLE.add(tag)

    # <note>
    tag = block_tag("note")
    tag.set_property("paragraph-background", "#f0f0f0")
    tag.set_property("foreground", "#0000ee")
    TAG_TABLE.add(tag)

    # <output>
    tag = inline_tag("output")
    tag.set_property("family", "monospace")
    TAG_TABLE.add(tag)

    # <p>
    tag = simple_block_tag("p")
    TAG_TABLE.add(tag)

    # <screen>
    tag = simple_block_tag("screen")
    TAG_TABLE.add(tag)

    # <synopsis>
    tag = block_tag("synopsis")
    TAG_TABLE.add(tag)

    # <sys>
    tag = inline_tag("sys")
    tag.set_property("family", "monospace")
    TAG_TABLE.add(tag)

    # <title>
    tag = simple_block_tag("title")
    tag.set_property("scale", pango.SCALE_XX_LARGE)
    tag.set_property("underline", True)
    TAG_TABLE.add(tag)

    # <var>
    tag = inline_tag("var")
    tag.set_property("style", pango.STYLE_ITALIC)
    TAG_TABLE.add(tag)

_setup_tags()
