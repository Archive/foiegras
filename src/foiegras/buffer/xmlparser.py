# Copyright (C) 2007 by Buddhika Laknath <blaknath@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import pygtk
pygtk.require('2.0')
import gtk
import xml.parsers.expat
import block_handler, table_handler, attribute_handler


class Parser:
    """
    XML parser for FoieGras
        
    Known tags are noted in orange
    unknown tags are noted in green
    """
    # keeps the current loaded tags(eg: tag0, tag1, ...) and elements(eg: em, code,..)
    tag_stack, elements = [], []
    show_tags = False
    temp_block_height = 0

    # table variables
    current_table = None #keeps track of the current table

    ## Table constraints
    # NO_TABLE = No table tag is found
    # TABLE_FOUND = Table tag is found and collecting info about the table to draw the table
    # TABLE_DRAWN = Table tag is closed and table is fully drawn
    table_cons = {"NO_TABLE":0, "TABLE_DEFINED":1, "TABLE_DRAWN":2} #table constraints
    table_status = table_cons["NO_TABLE"] #table status when parsing the document
    num_columns = 0;
    last_row = []
    
    def set_show_tags(self, state):
        self.show_tags = state
            
    def get_show_tags(self):
        return self.show_tags  

    def special_handler(self, element,attrs,tag_start,tag = None):
        if element == "info":
            pass

        if element == "title" and tag_start == True and    \
        self.title == "":
            self.title_parsing = True
        if element == "title" and tag_start == False:
            self.title_parsing = False

        
    
    def start_element(self, name, attrs):
        """
        whan a tag start occurse parser callse this handler
        """
        # create sentence using tag+attribute
        tag = name
        if len(attrs) != 0 :
            for k, v in attrs.iteritems():
                tag += " " + k + " = \" " +  v + "\" "

        # if parsed tag is in the valid tag list (for known tags)
        if name in self.main_buffer.tag_details.keys(): 

            # add parsed tag to tag stack
            # at a given moment tag stack holds the tags until the current tag         
            self.tag_stack.append(self.main_buffer.tag_details[name]["tag"])
            self.elements.append(name)       
  
            # handling control according to the tag's statues-block or inline
            if self.main_buffer.tag_details[name]["type"] == "table":
                table_handler.handler(self, name, attrs, True, tag)       
            elif self.main_buffer.tag_details[name]["type"] == "block":
                block_handler.handler(self, name, attrs, True, tag)
            elif self.main_buffer.tag_details[name]["type"] == "inline":
                inline_handler.handler(self, name, attrs, True, tag)  
            elif self.main_buffer.tag_details[name]["type"] == "special":
                self.special_handler(name, attrs, True, tag)


            # choose the current focused text view
            if self.main_buffer.inner_textviews:
                current_textview = self.main_buffer.inner_textviews[len(self.main_buffer.inner_textviews)-1]
            else:
                current_textview = self.main_buffer.main_textview        
            
            # validate tag attributes
            try:
                # check validity of attributes
                attribute_handler.attribute_handler(name, self.temp_buffer, self.main_buffer.tag_details[name]["attribs"], attrs, current_textview)                
            except KeyError, msg:
                # occur in an invalid key exception
                print "No such tag as", msg
                #exit()
            except attribute_handler.InvlidAttribute, msg:
                # occur in invalid value exception
                print msg
                #exit()                     
            except Exception, msg:
                print "Error:", msg
                #exit()
                      
        # for unknown tags
        else:
            print "Additional"
            iter1 =  self.temp_buffer.get_end_iter()               
            self.temp_buffer.insert_with_tags_by_name(iter1, "<"+tag+">", self.main_buffer.tag_details["tag"]["tag"])
            
    def end_element(self, name):
        """
        when a tag end occurs parser calls this handler    
        """
        if name in self.main_buffer.tag_details.keys() and len(self.tag_stack) != 0 : 
            self.tag_stack.pop()   
            previous_element = self.elements.pop()
            
            if self.main_buffer.tag_details[name]["type"] == "table":
                table_handler.handler(self, name, None , False)            
            elif self.main_buffer.tag_details[name]["type"] == "block":
                block_handler.handler(self, name, None, False)
            elif self.main_buffer.tag_details[name]["type"] == "inline":
                inline_handler.handler(self, name, None, False)   
            elif self.main_buffer.tag_details[name]["type"] == "special":
                self.special_handler(name, None, False)
        else:
            iter1 =  self.temp_buffer.get_end_iter()                  
            self.temp_buffer.insert_with_tags_by_name(iter1, "</"+name+">", self.main_buffer.tag_details["tag"]["tag"])    
    
    def char_data(self, data):
        """
        when a character text occurs parser callse this handler
        """
        if self.title_parsing:
            self.title += data
        if self.table_status == self.table_cons["NO_TABLE"]:  
            iter1 =  self.temp_buffer.get_end_iter()                          
            self.temp_buffer.insert_with_tags_by_name(iter1, data, *self.tag_stack)
        else:
            # if a new column comes, append the column data to the array
            # which will be later added to the liststore of the table
            if self.last_row.__len__() < 4 and data.strip() != "":
                self.last_row.append( data )               

    
    def __init__(self, buffer, show_tags= False):

        self.set_show_tags(show_tags)

        self.title_parsing = False
        self.title = ""
        
        p = xml.parsers.expat.ParserCreate()
        p.StartElementHandler = self.start_element
        p.EndElementHandler = self.end_element
        p.CharacterDataHandler = self.char_data

        if buffer.__class__.__name__ == "MallardBuffer" :
            # main_buffer is the outmost textbuffer and the
            # buffer of main textview.
            # temp_buffer keeps current focused inner text buffer 
            self.temp_buffer = self.main_buffer = buffer
            self.x, self.y = 400, 10
            
            #read the text in buffer
            iter1, iter2 = self.main_buffer.get_bounds()
            page = self.main_buffer.get_text(iter1, iter2)

            #format the buffer
            self.main_buffer.set_text("") 
            #self.set_show_tags(False)

            try:
                p.Parse(page)
            except xml.parsers.expat.ExpatError, msg:
                print "Foiegras Error: ", msg
                exit()

        print "Title: "+str(self.title)
