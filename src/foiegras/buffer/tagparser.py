# Copyright (C) 2007 by Buddhika Laknath <blaknath@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import pygtk
pygtk.require('2.0')
import gtk,sys
import xml.parsers.expat, pango


class TagParser:
    """
    Tag parser for FoieGras
        
    Reads tags from a external config file
    and inserts them to the Text Tag Table
    """
    # 3 handler functions
    def start_element(self,name,attrs):
        """whan a tag start occurse parser callse this handler"""
        if name == "tag":      
            # keep track of the parent tag
            self.tags_stack.append(attrs["name"])

            # keep track of last attribut passed
            # for the use of char_data function 
            self.last_attrib = None    

            # keep track of attribute list 
            # for a parent tag
            self.attr_list = {}                    
        else:
            self.last_attrib = name.strip()
                             
        # avoid duplicates       
        if "name" in attrs and attrs["name"] not in self.buffer.tag_details:
            name = attrs["name"]
            self.buffer.tag_details[name] = attrs

            if attrs["type"] != "special":
                styles = eval( "dict(" + self.buffer.tag_details[name]["style"] + ")" )
            else:
                styles = {}

            #creats a tag in text_tag_table of the buffer
            self.buffer.create_tag("tag"+ str(self.tag_count),**styles )

            #put the name of the tag in dict
            self.buffer.tag_details[name]["tag"]= "tag"+ str(self.tag_count)
            self.tag_count += 1          

    def char_data(self,data):
        if self.last_attrib != None and data.strip() != "":
            self.attr_list[self.last_attrib] = data
   
    def end_element(self,name):
        if name == "tag":
            try:
                tag = self.tags_stack.pop()   
                self.buffer.tag_details[tag]["attribs"] = self.attr_list
            except Exception,msg:
                print msg
                exit()

    def __init__(self,buffer,page):
        self.tag_count = 0;
        self.buffer = buffer 
  
        #track tag attributes
        self.attr_list = {}
        self.tags_stack = []          
        self.last_attrib = None      

        #creating a xmlparser
        p = xml.parsers.expat.ParserCreate()
        p.StartElementHandler = self.start_element
        p.CharacterDataHandler = self.char_data  
        p.EndElementHandler = self.end_element              
                
        if buffer.__class__.__name__ == "MallardBuffer" :
            try:
                p.Parse( page )
            except xml.parsers.expat.ExpatError,msg:
                print "Foiegras Error: ",msg
                exit()
            
