# Copyright (C) 2007 by Buddhika Laknath <blaknath@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import pygtk
pygtk.require('2.0')
import gtk
import docbuffer

from tags import SIMPLE_BLOCK_TAGS

def block_handler(buffer, tag_name, block_iter = None, new_block = True):
    """
    Hanlder for block tags.
    Maintains the working of nested block tags
    
    If some text is selected, a block will be created from selected 
    text. If not, the text starting from current cursor point will
    turn to a block. 
    """   
    if not block_iter:
        block_mark = buffer.get_insert()
        block_iter = buffer.get_iter_at_mark(block_mark)
        
    if not block_iter.starts_line():
        if new_block:
            buffer.insert(block_iter,"\n")
            block_iter = buffer.get_iter_at_mark(buffer.get_insert())
        else:
            block_iter.set_line_offset(0)
   
    start_offset = block_iter.get_offset()

    end_iter = buffer.get_iter_at_mark(buffer.get_selection_bound())
    end_offset = end_iter.get_offset()
    
    if not buffer.get_has_selection():
        if not end_iter.ends_line():
            end_iter.forward_to_line_end()
            end_iter.forward_char()
        end_offset = end_iter.get_offset()
    else:
        end_offset = end_offset+1        
        buffer.insert(end_iter,"\n")
        
    start_iter = buffer.get_iter_at_offset(start_offset)
    end_iter = buffer.get_iter_at_offset(end_offset)

    if tag_name in SIMPLE_BLOCK_TAGS:
        for t in SIMPLE_BLOCK_TAGS:
            buffer.remove_tag_by_name(t, start_iter, end_iter)

    buffer.apply_tag_by_name(tag_name, start_iter, end_iter)
    
    
