# Copyright (C) 2007 by Szilveszter Farkas (Phanatic) <szilveszter.farkas@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import pygtk
pygtk.require('2.0')
import gtk

from foiegras.windows.main import MainWindow
from foiegras.preferences import FoieGrasPreferencesGconf

class FoieGrasApplication:
    """
    This is the main application class for Foie Gras.
    """
    def __init__(self, datadir):
        """ Constructor for the application class. """
        self._conf = FoieGrasPreferencesGconf()
        self._datadir = datadir
        self._document = None
        
        self._info = None
        self._properties = None
        self._vcs = None
        
        # Initialize GtkRecentManager
        self.recent_manager = gtk.recent_manager_get_default()
        self.recent_data = { 'mime_type': 'text/plain',
                             'app_name': 'Foie Gras',
                             'app_exec': 'start-foiegras' }
        self.recent_filter = gtk.RecentFilter()
        self.recent_filter.add_application('Foie Gras')
        
        self._main = MainWindow(self)
        
        self._main.show()
    
    def get_conf(self):
        """ Return the Gconf backend object. """
        return self._conf
    
    def get_datadir(self):
        """ Return the path to the data directory. """
        return self._datadir
    
    def get_document(self):
        """ Return the Document object. """
        return self._document
    
    def get_vcs(self):
        """ Return the VersionControl object. """
        return self._vcs
    
    def set_document(self, document):
        """ Set the Document object. """
        self._document = document
    
    def set_vcs(self, vcs):
        """ Set the VersionControl object. """
        self._vcs = vcs
