# Copyright (C) 2007 by Szilveszter Farkas (Phanatic) <szilveszter.farkas@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import os.path

import gconf

GCONF_ROOT = '/apps/foiegras/'

class FoieGrasPreferencesGconf:
    """ Class that implements the Gconf backend for Foie Gras. """
    def __init__(self):
        """ Initialize the Gconf backend. """
        # Get the default client instance
        self._client = gconf.client_get_default()
        # Add the main directory for Foie Gras
        self._client.add_dir(GCONF_ROOT[:-1], gconf.CLIENT_PRELOAD_NONE)
    
    def add_notify(self, key, callback):
        """
        Sets up a notification callback.
        """
        keypath = GCONF_ROOT + key
        self._client.notify_add(keypath, callback)
    
    def add_vcs_profile(self, data):
        """
        Add a new VCS profile.
        
        The data is a dictionary with the following format:
        id:       the ID of the profile
        name:     human readable name of the profile
        type:     type of the VCS (supported: bazaar, subversion)
        uri:      repository location (local or remote, full path)
        readonly: true if repository is read-only
        
        Return True if successfully added, otherwise return False.
        
        Note: you can use this method to modify VCS profiles (just specify an
        existing ID).
        """
        try:
            keypath = GCONF_ROOT + 'vcsprofiles/' + data['id'] + '/'
        except KeyError:
            return False
        
        try:
            self._client.set_string(keypath + 'name', data['name'])
            self._client.set_string(keypath + 'type', data['type'])
            self._client.set_string(keypath + 'uri', data['uri'])
            self._client.set_bool(keypath + 'readonly', data['readonly'])
        except KeyError:
            return False
        
        return True
    
    def exists_vcs_profile(self, id):
        """
        Checks whether a VCS profile exists with the given ID.
        
        Returns True or False.
        """
        keypath = GCONF_ROOT + 'vcsprofiles/' + id
        
        val = self._client.get_string(keypath + '/name')
        if val is None:
            # We assume that there is no such profile
            return False
        else:
            return True
    
    def get_preference(self, key, type, default):
        """
        Return the value of the given key, or the default, if not specified.
        
        Types supported: bool, int, string
        """
        val = None
        keypath = GCONF_ROOT + key
        
        if type == 'bool':
            val = self._client.get_bool(keypath)
        elif type == 'int':
            val = self._client.get_int(keypath)
            if val == 0:
                val = default
                self._client.set_int(keypath, val)
        elif type == 'string':
            val = self._client.get_string(keypath)
            if val is None:
                val = default
                self._client.set_string(keypath, val)
        
        return val
    
    def get_vcs_profile(self, id):
        """
        Return the preferences of the given VCS profile as a directory.
        """
        keypath = GCONF_ROOT + 'vcsprofiles/' + id
        data = {}
        
        val = self._client.get_string(keypath + '/name')
        if val is None:
            # We assume that there is no such profile
            return None
        else:
            data['id'] = id
            data['name'] = val
            data['type'] = self._client.get_string(keypath + '/type')
            data['uri'] = self._client.get_string(keypath + '/uri')
            data['readonly'] = self._client.get_bool(keypath + '/readonly')
            return data
    
    def get_vcs_profiles(self):
        """
        Return the list of VCS profiles as a list of directories.
        """
        keypath = GCONF_ROOT + 'vcsprofiles'
        profiles = []
        
        for id in self._client.all_dirs(keypath):
            # We need the final part only
            id = os.path.basename(id)
            # Fill up with data
            data = { 'id': id }
            data['name'] = self._client.get_string(keypath + '/' + id + '/name')
            data['type'] = self._client.get_string(keypath + '/' + id + '/type')
            data['uri'] = self._client.get_string(keypath + '/' + id + '/uri')
            data['readonly'] = self._client.get_bool(keypath + '/' + id + '/treadonly')
            # Add to the profile list
            profiles.append(data)
        
        return profiles
    
    def remove_vcs_profile(self, id):
        """
        Remove the VCS profile with the given ID.
        """
        keypath = GCONF_ROOT + 'vcsprofiles/' + id + '/'
        
        self._client.unset(keypath + 'name')
        self._client.unset(keypath + 'type')
        self._client.unset(keypath + 'uri')
        self._client.unset(keypath + 'readonly')
        self._client.unset(keypath[:-1])
    
    def set_preference(self, key, type, value):
        """
        Set the value for the given key.
        
        Types supported: bool, int, string
        """
        keypath = GCONF_ROOT + key
        
        if type == 'bool':
            self._client.set_bool(keypath, value)
        elif type == 'int':
            self._client.set_int(keypath, value)
        elif type == 'string':
            self._client.set_string(keypath, value)
